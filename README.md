# OData Schema

Parse OData schemas into Rust types, with the goal of supporting Dynamics 365 OData operations.

## Roadmap

https://en.wikipedia.org/wiki/Open_Data_Protocol#Architecture

- [x] Deserialise `$metadata` document
- [ ] Construct coherent entity model
    - [ ] References/includes
    - [ ] Annotations/annotation groups (annotations currently not constructed for "completed" items
    either)
    - [x] Types
    - [ ] Actions
    - [ ] Functions
    - [ ] Terms
    - [ ] Entity containers
        - [x] Entity sets
        - [ ] Action imports
        - [ ] Function imports
        - [x] Singletons
        - [ ] Extends
    - [ ] Customisation/handling of metadata inconsistencies
        - [ ] User-specified handling of qualified name/target path resolution errors
- [ ] Code generation
    - [x] Type definitions
    - [x] Entity container definitions
    - [x] Entity set definitions
    - [ ] Actions
    - [ ] Functions
    - [ ] Black/whitelist for generated types (with stubbed dependencies)
- [ ] Client functionality
    - [x] Retrieve
    - [ ] Filter/queries
    - [ ] Create/Update/Delete
    - [ ] Call actions & functions
    - [ ] ETag support
    - [ ] Retrieve/set open types
    - Batch requests
- [ ] Consuming crate bindings
    - [ ] Proc macro
    - [x] Proc macro attribute
    - [x] `build.rs` code generation
- [ ] What-if/dry-run