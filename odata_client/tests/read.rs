//! Integration tests which involve read-only requests

mod trippin_restier_entity_model;

use trippin_restier_entity_model::{get_endpoints, Endpoints};

// TODO #2: use mock client with prepared responses for offline tests

#[tokio::test]
async fn retrieve_entity_set_succeeds() {
    let client = get_client();
    let Endpoints { people, .. } = get_endpoints().await;

    people.retrieve(&client).await.unwrap();
}

#[tokio::test]
async fn retrieve_entity_set_gets_all_pages() {
    let client = get_client();
    let Endpoints { people, .. } = get_endpoints().await;

    let entity_set = people.retrieve(&client).await.unwrap();

    assert_eq!(
        20,
        entity_set.len(),
        "Retrieved entity set count is incorrect"
    )
}

#[tokio::test]
async fn retrieve_entity_from_set_succeeds() {
    let client = get_client();
    let Endpoints { people, .. } = get_endpoints().await;

    people
        .retrieve_entity(&client, "russellwhyte")
        .await
        .unwrap();
}

#[tokio::test]
async fn retrieve_singleton_entity_succeeds() {
    let client = get_client();
    let Endpoints { me, .. } = get_endpoints().await;

    me.get(&client).await.unwrap();
}

fn get_client() -> reqwest::Client {
    reqwest::Client::builder()
        .danger_accept_invalid_certs(true)
        .build()
        .unwrap()
}
