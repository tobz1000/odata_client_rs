use chrono::{DateTime, FixedOffset};
use iso8601::Duration;
use odata_client::{EntityLink, EntityProperties, EntitySetEndpoint, SingletonEndpoint};
use odata_client_util::deserialize_with::Iso8601DurationDeserialize;
use serde::{Deserialize, Serialize};
use serde_with::serde_as;
use std::marker::PhantomData;
use uuid::Uuid;

/// Redirects to an ephemeral read-write service URL
const SERVICE_SEED_URL: &str = "https://services.odata.org/TripPinRESTierService/";

pub struct Endpoints {
    pub people: EntitySetEndpoint<Person, String>,
    pub me: SingletonEndpoint<Person, String>,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "PascalCase")]
pub struct Person {
    pub user_name: String,
    pub first_name: String,
    pub last_name: Option<String>,
    pub middle_name: Option<String>,
    pub gender: PersonGender,
    pub age: Option<i64>,
    pub emails: Vec<String>,
    pub address_info: Vec<Location>,
    pub home_address: Option<Location>,
    pub favorite_feature: Feature,
    pub features: Vec<Feature>,
    pub friends: Vec<EntityLink<Person>>,
    pub best_friend: Option<EntityLink<Person>>,
    pub trips: Vec<Trip>, // This is a contained set despite the schema saying otherwise
}

impl EntityProperties for Person {
    const EXPAND_QUERY: Option<&'static str> =
        Some("Friends/$ref,BestFriend/$ref,Trips($expand=PlanItems)");
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub enum PersonGender {
    Male = 0,
    Female = 1,
    Unknown = 2,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
pub enum Feature {
    Feature1 = 0,
    Feature2 = 1,
    Feature3 = 2,
    Feature4 = 3,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "PascalCase")]
pub struct Location {
    pub address: Option<String>,
    pub city: Option<City>,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "PascalCase")]
pub struct City {
    pub name: Option<String>,
    pub country_region: Option<String>,
    pub region: Option<String>,
}

#[derive(Debug, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "PascalCase")]
pub struct Trip {
    trip_id: i32,
    share_id: Uuid,
    name: Option<String>,
    budget: f32,
    description: Option<String>,
    tags: Vec<String>,
    starts_at: DateTime<FixedOffset>,
    ends_at: DateTime<FixedOffset>,
    plan_items: Vec<PlanItem>, // This is a contained set despite the schema saying otherwise
}

impl EntityProperties for Trip {
    const EXPAND_QUERY: Option<&'static str> = Some("PlanItems");
}

#[serde_as]
#[derive(Debug, Deserialize, Serialize, PartialEq)]
#[serde(rename_all = "PascalCase")]
struct PlanItem {
    plan_item_id: i32,
    confirmation_code: Option<String>,
    starts_at: Option<DateTime<FixedOffset>>,
    ends_at: Option<DateTime<FixedOffset>>,
    #[serde_as(as = "Option<Iso8601DurationDeserialize>")]
    duration: Option<Duration>,
}

impl EntityProperties for PlanItem {
    const EXPAND_QUERY: Option<&'static str> = None;
}

pub async fn get_endpoints() -> Endpoints {
    let seed_resp = reqwest::get(SERVICE_SEED_URL).await.unwrap();

    let people = EntitySetEndpoint {
        service_url: seed_resp.url().to_string(),
        name: "People".to_string(),
        marker: PhantomData,
    };

    let me = SingletonEndpoint {
        service_url: seed_resp.url().to_string(),
        name: "Me".to_string(),
        marker: PhantomData,
    };

    Endpoints { people, me }
}
