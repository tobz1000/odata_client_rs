//! Integration tests which involve persistent write requests.

// TODO #2: use mock client with prepared responses for offline tests

mod trippin_restier_entity_model;

use odata_client::EntityLink;
use trippin_restier_entity_model::{
    get_endpoints, City, Endpoints, Feature, Location, Person, PersonGender,
};

// TODO #2: Example service returns entity set as response to create request, instead of just the
// created entity, which is wrong AFAIK.
// TODO #4: handle collection nav props (empty or not) with appropriate link modifications
#[tokio::test]
async fn create_entity_succeeds() {
    let client = get_client();
    let Endpoints { people, .. } = get_endpoints().await;

    let to_create = Person {
        user_name: "sperson".to_string(),
        first_name: "Some".to_string(),
        last_name: Some("Person".to_string()),
        middle_name: None,
        gender: PersonGender::Male,
        age: Some(1_001),
        emails: vec!["sperson@fake.mail".to_string()],
        address_info: vec![Location {
            address: Some("123 Fake Street".to_string()),
            city: Some(City {
                country_region: Some("USA".to_string()),
                name: Some("Faketown".to_string()),
                region: None,
            }),
        }],
        home_address: None,
        favorite_feature: Feature::Feature1,
        features: vec![Feature::Feature1, Feature::Feature2],
        friends: vec![],
        best_friend: None,
        trips: vec![],
    };

    let created = people.create_entity(&client, &to_create).await.unwrap();

    assert_eq!(created.properties, to_create);
}

#[tokio::test]
async fn create_entity_with_entity_links_succeeds() {
    let mut client = get_client();
    let Endpoints { people, .. } = get_endpoints().await;

    let friend = people
        .retrieve(&client)
        .await
        .unwrap()
        .first()
        .unwrap()
        .to_entity_link()
        .unwrap();

    let to_create = Person {
        user_name: "sperson".to_string(),
        first_name: "Some".to_string(),
        last_name: Some("Person".to_string()),
        middle_name: None,
        gender: PersonGender::Male,
        age: Some(1_001),
        emails: vec!["sperson@fake.mail".to_string()],
        address_info: vec![Location {
            address: Some("123 Fake Street".to_string()),
            city: Some(City {
                country_region: Some("USA".to_string()),
                name: Some("Faketown".to_string()),
                region: None,
            }),
        }],
        home_address: None,
        favorite_feature: Feature::Feature1,
        features: vec![Feature::Feature1, Feature::Feature2],
        friends: vec![friend.clone()],
        best_friend: Some(friend.clone()),
        trips: vec![],
    };

    let created = people.create_entity(&mut client, &to_create).await.unwrap();

    assert_eq!(created.properties.friends, &[friend.clone()]);
    assert_eq!(created.properties.best_friend, Some(friend.clone()));
}

#[tokio::test]
async fn delete_entity_succeeds() {
    todo!()
    // let client = get_client();
    // let Endpoints { people, .. } = get_endpoints().await;

    // let to_delete = EntityLink::<Person> {
    //     marker: PhantomData,
    //     entity_path: "https://services.odata.org/TripPinRESTierService/People('russellwhyte')"
    //         .to_string(),
    // };

    // let delete_resp = dbg!(to_delete.get(&client).await);

    // assert!(delete_resp.is_ok());

    // to_delete.delete(&client).await.unwrap();

    // assert!(to_delete.get(&client).await.is_err());
}

fn get_client() -> reqwest::Client {
    reqwest::Client::builder()
        .danger_accept_invalid_certs(true)
        .build()
        .unwrap()
}
