//! Retrieves all fields of a single entity record.

use std::marker::PhantomData;

use super::ODataInteraction;
use crate::{
    path::{AsStr, EntityPath},
    requests::retrieve_single::{RetrieveSingle, RetrievedEntity},
    EntityProperties,
};
use async_trait::async_trait;
use serde::Deserialize;

pub struct RetrieveSingleFull<S: AsStr, T: EntityProperties> {
    entity_path: EntityPath<S>,
    marker: PhantomData<T>,
}

#[async_trait]
impl<S: AsStr, T: EntityProperties + for<'de> Deserialize<'de> + Send + Sync> ODataInteraction
    for RetrieveSingleFull<S, T>
{
    type Output = RetrievedEntity<T>;

    async fn perform<C: crate::client::http_client::ODataHttpClient>(
        &self,
        client: &C,
    ) -> Result<Self::Output, anyhow::Error>
    where
        for<'b> &'b C: Send,
    {
        let retrieve_spec = RetrieveSingle::<&str, T> {
            entity_path: (&self.entity_path).into(),
            marker: PhantomData::<T>,
            expand_query: T::EXPAND_QUERY,
        };

        retrieve_spec.perform(client).await
    }
}
