//! Retrieves all fields of multiple records.

use async_trait::async_trait;
use serde::Deserialize;
use std::marker::PhantomData;

use crate::{
    path::{AsStr, EntitySetPath},
    requests::retrieve_multiple::{EntitySetPayload, RetrieveMultiple},
    EntityProperties,
};

use super::ODataInteraction;

pub struct RetrieveMultipleFull<S: AsStr, T> {
    pub entity_set_path: EntitySetPath<S>,
    pub filter_query: Option<S>,
    pub marker: PhantomData<T>,
}

#[async_trait]
impl<S: AsStr, T: EntityProperties + for<'de> Deserialize<'de> + Send + Sync> ODataInteraction
    for RetrieveMultipleFull<S, T>
{
    type Output = EntitySetPayload<T>;

    async fn perform<C: crate::client::http_client::ODataHttpClient>(
        &self,
        client: &C,
    ) -> Result<Self::Output, anyhow::Error>
    where
        for<'b> &'b C: Send,
    {
        let retrieve_spec = RetrieveMultiple::<&str, T> {
            entity_set_path: (&self.entity_set_path).into(),
            filter_query: self.filter_query.as_ref().map(|s| s.as_ref()),
            expand_query: T::EXPAND_QUERY,
            marker: PhantomData::<T>,
        };

        retrieve_spec.perform(client).await
    }
}
