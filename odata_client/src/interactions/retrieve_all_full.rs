use super::{retrieve_multiple_full::RetrieveMultipleFull, ODataInteraction};
use crate::{
    client::http_client::ODataHttpClient, path::AsStr, requests::retrieve_single::RetrievedEntity,
    EntityProperties,
};
use async_trait::async_trait;
use http::Method;
use reqwest::{Request, Url};
use serde::Deserialize;

pub struct RetrieveAllFull<S: AsStr, T> {
    request_spec: RetrieveMultipleFull<S, T>,
}

#[async_trait]
impl<S: AsStr, T: EntityProperties + for<'de> Deserialize<'de> + Send + Sync> ODataInteraction
    for RetrieveAllFull<S, T>
{
    type Output = Vec<RetrievedEntity<T>>;

    async fn perform<C: ODataHttpClient>(&self, client: &C) -> Result<Self::Output, anyhow::Error>
    where
        for<'b> &'b C: Send,
    {
        let mut entities = Vec::new();
        let mut resp_payload = self.request_spec.perform(client).await?;

        loop {
            entities.append(&mut resp_payload.value);

            if let Some(next_link) = resp_payload.next_link {
                // odata-json-format-v4.0-cs01 sec 4.5.5: `next_link` should be a full URL, no need
                // to augment with context
                let next_url = Url::parse(&next_link.0)?;
                let req = Request::new(Method::GET, next_url);
                let resp = client.execute_request(req).await?.into();
                let text = resp.text().await?;
                resp_payload = serde_json::from_str(&text)?;
            } else {
                return Ok(entities);
            }
        }
    }
}
