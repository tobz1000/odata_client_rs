use async_trait::async_trait;

/// Used to send HTTP requests for OData operations. A custom implementation may be used in order to
/// e.g. add authentication headers before sending the request.
#[async_trait]
pub trait ODataHttpClient {
    type Response: Into<reqwest::Response>;
    type Error: std::error::Error + Send + Sync + 'static;

    /// Takes an HTTP request prepared for a specific OData operation, makes any necessary
    /// modifications specific to this client, and performs the HTTP request.
    async fn execute_request(&self, req: reqwest::Request) -> Result<Self::Response, Self::Error>;
}

#[async_trait]
impl ODataHttpClient for reqwest::Client {
    type Response = reqwest::Response;
    type Error = reqwest::Error;

    async fn execute_request(
        &self,
        request: reqwest::Request,
    ) -> Result<reqwest::Response, reqwest::Error> {
        self.execute(request).await
    }
}
