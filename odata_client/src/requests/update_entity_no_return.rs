use super::{update_entity::UpdateEntityPayload, ODataRequest};
use crate::path::{AsStr, EntityPath};
use serde::Serialize;

/// Retrieves multiple entities from a set.
pub struct UpdateEntityNoReturn<'a, S: AsStr, T> {
    entity_path: EntityPath<S>,
    to_update: UpdateEntityPayload<'a, T>,
}

impl<'a, 'de, S: AsStr, T: Serialize> ODataRequest for UpdateEntityNoReturn<'a, S, T> {
    type Req = UpdateEntityPayload<'a, T>;
    type Resp = ();
    const HTTP_METHOD: http::Method = http::Method::PATCH;

    fn headers(&self) -> &[(&'static str, &str)] {
        const REQ_HEADERS: &'static [(&str, &str)] = &[
            ("Content-Type", "application/json"),
            ("Accept", "application/json;odata.metadata=full"),
            ("OData-Version", "4.0"),
            ("Prefer", "return=minimal"),
        ];
        REQ_HEADERS
    }

    fn url(&self) -> String {
        self.entity_path.to_string()
    }

    fn body(&self) -> &Self::Req {
        &self.to_update
    }
}
