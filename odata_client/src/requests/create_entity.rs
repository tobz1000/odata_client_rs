use super::{retrieve_single::RetrievedEntity, ODataRequest};
use crate::path::{AsStr, EntitySetPath};
use serde::{Deserialize, Serialize};

/// Retrieves multiple entities from a set.
pub struct CreateEntity<'a, S: AsStr, T> {
    entity_set_path: EntitySetPath<S>,
    to_create: CreateEntityPayload<'a, T>,
}

impl<'a, S: AsStr, T: for<'de> Deserialize<'de> + Serialize> ODataRequest
    for CreateEntity<'a, S, T>
{
    type Req = CreateEntityPayload<'a, T>;
    type Resp = RetrievedEntity<T>;
    const HTTP_METHOD: http::Method = http::Method::POST;

    fn headers(&self) -> &[(&'static str, &str)] {
        const REQ_HEADERS: &'static [(&str, &str)] = &[
            ("Content-Type", "application/json"),
            ("Accept", "application/json;odata.metadata=full"),
            ("OData-Version", "4.0"),
            ("Prefer", "return=representation"),
        ];
        REQ_HEADERS
    }

    fn url(&self) -> String {
        self.entity_set_path.to_string()
    }

    fn body(&self) -> &Self::Req {
        &self.to_create
    }
}

#[derive(Debug, Serialize)]
pub struct CreateEntityPayload<'a, T> {
    #[serde(rename = "@odata.type")]
    #[serde(skip_serializing_if = "Option::is_none")]
    pub entity_type: Option<&'a str>,
    #[serde(flatten)]
    pub properties: &'a T,
}
