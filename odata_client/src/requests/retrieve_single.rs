use super::ODataRequest;
use crate::path::{AsStr, ContextUrl, EntityPath};
use serde::Deserialize;
use std::marker::PhantomData;

/// Retrieves a single entity from a set by key.
pub struct RetrieveSingle<S: AsStr, T> {
    pub entity_path: EntityPath<S>,
    pub expand_query: Option<S>,
    pub marker: PhantomData<T>,
}

impl<S: AsStr, T: for<'de> Deserialize<'de>> ODataRequest for RetrieveSingle<S, T> {
    type Req = ();
    type Resp = RetrievedEntity<T>;
    const HTTP_METHOD: http::Method = http::Method::GET;

    fn headers(&self) -> &[(&'static str, &str)] {
        &[
            ("Content-Type", "application/json"),
            ("Accept", "application/json;odata.metadata=full"),
        ]
    }

    fn url(&self) -> String {
        let mut url = self.entity_path.to_string();

        if let Some(expand) = self.expand_query.as_ref() {
            url.push_str("?$expand=");
            url.push_str(expand.as_ref());
        }

        url
    }

    fn body(&self) -> &Self::Req {
        &()
    }
}

#[derive(Debug, Deserialize)]
pub struct RetrievedEntity<T> {
    #[serde(rename = "@odata.context")]
    pub context: ContextUrl<String>,
    #[serde(rename = "@odata.id")]
    pub id: EntityPath<String>,
    #[serde(rename = "@odata.etag")]
    pub etag: Option<String>,
    #[serde(rename = "@odata.editLink")]
    pub edit_link: EntityPath<String>,
    #[serde(flatten)]
    pub properties: T,
}
