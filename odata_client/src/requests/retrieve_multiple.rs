use std::marker::PhantomData;

use super::{retrieve_single::RetrievedEntity, ODataRequest};
use crate::path::{AsStr, ContextUrl, EntityPath, EntitySetPath};
use serde::Deserialize;

/// Retrieves multiple entities from a set.
pub struct RetrieveMultiple<S: AsStr, T> {
    pub entity_set_path: EntitySetPath<S>,
    pub filter_query: Option<S>,
    pub expand_query: Option<S>,
    pub marker: PhantomData<T>,
}

impl<S: AsStr, T: for<'de> Deserialize<'de>> ODataRequest for RetrieveMultiple<S, T> {
    type Req = ();
    type Resp = EntitySetPayload<T>;
    const HTTP_METHOD: http::Method = http::Method::GET;

    fn headers(&self) -> &[(&'static str, &str)] {
        &[
            ("Content-Type", "application/json"),
            ("Accept", "application/json;odata.metadata=full"),
        ]
    }

    fn url(&self) -> String {
        let mut url = self.entity_set_path.to_string();

        let mut first_query_param = true;

        let mut write_param = |name, val| {
            if let Some(val) = val {
                url.push(if first_query_param { '?' } else { '&' });
                url.push_str(name);
                url.push('=');
                url.push_str(val);

                first_query_param = false;
            }
        };

        write_param("$filter", self.filter_query.as_ref().map(|s| s.as_ref()));
        write_param("$expand", self.expand_query.as_ref().map(|s| s.as_ref()));

        url
    }

    fn body(&self) -> &Self::Req {
        &()
    }
}

#[derive(Debug, Deserialize)]
pub struct EntitySetPayload<P> {
    #[serde(rename = "@odata.context")]
    pub context: ContextUrl<String>,
    #[serde(rename = "@odata.nextLink")]
    pub next_link: Option<EntityPath<String>>,
    #[serde(flatten)]
    pub value: Vec<RetrievedEntity<P>>,
}
