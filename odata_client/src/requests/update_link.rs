use super::ODataRequest;
use crate::path::{AsStr, EntityPath, EntityPropPath};
use serde::Serialize;

/// Retrieves multiple entities from a set.
pub struct UpdateLink<S: AsStr> {
    entity_prop_path: EntityPropPath<S>,
    to_create: UpdateLinkPayload<S>,
}

impl<S: AsStr> ODataRequest for UpdateLink<S> {
    type Req = UpdateLinkPayload<S>;
    type Resp = ();
    const HTTP_METHOD: http::Method = http::Method::PUT;

    fn headers(&self) -> &[(&'static str, &str)] {
        const REQ_HEADERS: &'static [(&str, &str)] = &[
            ("Content-Type", "application/json"),
            ("Accept", "application/json;odata.metadata=full"),
            ("OData-Version", "4.0"),
        ];
        REQ_HEADERS
    }

    fn url(&self) -> String {
        self.entity_prop_path.to_string()
    }

    fn body(&self) -> &Self::Req {
        &self.to_create
    }
}

#[derive(Debug, Serialize)]
pub struct UpdateLinkPayload<S: AsStr> {
    #[serde(rename = "@odata.id")]
    pub id: EntityPath<S>,
}
