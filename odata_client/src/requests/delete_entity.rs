use super::ODataRequest;
use crate::path::{AsStr, EntityPath};

/// Retrieves multiple entities from a set.
pub struct DeleteEntity<S: AsStr> {
    entity_path: EntityPath<S>,
}

impl<S: AsStr> ODataRequest for DeleteEntity<S> {
    type Req = ();
    type Resp = ();
    const HTTP_METHOD: http::Method = http::Method::DELETE;

    fn headers(&self) -> &[(&'static str, &str)] {
        &[]
    }

    fn url(&self) -> String {
        self.entity_path.to_string()
    }

    fn body(&self) -> &Self::Req {
        &()
    }
}
