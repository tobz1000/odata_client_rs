use super::{retrieve_single::RetrievedEntity, ODataRequest};
use crate::path::{AsStr, EntityPath};
use serde::{Deserialize, Serialize};

/// Retrieves multiple entities from a set.
pub struct UpdateEntity<'a, S: AsStr, T> {
    entity_path: EntityPath<S>,
    to_update: UpdateEntityPayload<'a, T>,
}

impl<'a, S: AsStr, T: for<'de> Deserialize<'de> + Serialize> ODataRequest
    for UpdateEntity<'a, S, T>
{
    type Req = UpdateEntityPayload<'a, T>;
    type Resp = RetrievedEntity<T>;
    const HTTP_METHOD: http::Method = http::Method::PATCH;

    fn headers(&self) -> &[(&'static str, &str)] {
        const REQ_HEADERS: &'static [(&str, &str)] = &[
            ("Content-Type", "application/json"),
            ("Accept", "application/json;odata.metadata=full"),
            ("OData-Version", "4.0"),
            ("Prefer", "return=representation"),
        ];
        REQ_HEADERS
    }

    fn url(&self) -> String {
        self.entity_path.to_string()
    }

    fn body(&self) -> &Self::Req {
        &self.to_update
    }
}

#[derive(Debug, Serialize)]
pub struct UpdateEntityPayload<'a, T> {
    #[serde(rename = "@odata.type")]
    pub entity_type: Option<&'a str>,
    #[serde(flatten)]
    pub properties: &'a T,
}
