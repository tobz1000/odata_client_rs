use super::ODataRequest;
use crate::path::{AsStr, EntityPropPath};

/// Retrieves multiple entities from a set.
pub enum DeleteLink<S: AsStr> {
    FromSingle {
        prop_path: EntityPropPath<S>,
    },
    FromCollection {
        prop_path: EntityPropPath<S>,
        key: S,
    },
}

impl<S: AsStr> ODataRequest for DeleteLink<S> {
    type Req = ();
    type Resp = ();
    const HTTP_METHOD: http::Method = http::Method::DELETE;

    fn headers(&self) -> &[(&'static str, &str)] {
        &[]
    }

    fn url(&self) -> String {
        match self {
            DeleteLink::FromSingle { prop_path } => prop_path.to_string(),
            DeleteLink::FromCollection { prop_path, key } => {
                format!("{}?$id={}", prop_path, key.as_ref())
            }
        }
    }

    fn body(&self) -> &Self::Req {
        &()
    }
}
