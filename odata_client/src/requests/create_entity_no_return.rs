use super::{create_entity::CreateEntityPayload, ODataRequest};
use crate::path::{AsStr, EntitySetPath};
use serde::Serialize;

/// Retrieves multiple entities from a set.
pub struct CreateEntityNoReturn<'a, S: AsStr, T> {
    entity_set_path: EntitySetPath<S>,
    to_create: CreateEntityPayload<'a, T>,
}

impl<'a, S: AsStr, T: Serialize> ODataRequest for CreateEntityNoReturn<'a, S, T> {
    type Req = CreateEntityPayload<'a, T>;
    type Resp = ();
    const HTTP_METHOD: http::Method = http::Method::POST;

    fn headers(&self) -> &[(&'static str, &str)] {
        const REQ_HEADERS: &'static [(&str, &str)] = &[
            ("Content-Type", "application/json"),
            ("Accept", "application/json;odata.metadata=full"),
            ("OData-Version", "4.0"),
            ("Prefer", "return=minimal"),
        ];
        REQ_HEADERS
    }

    fn url(&self) -> String {
        self.entity_set_path.to_string()
    }

    fn body(&self) -> &Self::Req {
        &self.to_create
    }
}
