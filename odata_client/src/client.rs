/* TODO:
- Retrieve multiple
    - Query options (#3)
        - filter, orderby, top, skip, count, expand, select, search, lambdas (`any`/`all`)
        - Supporting these would only be for query efficiency; doing most of this manipulation
        client-side is likely preferable. $filter would be the most useful.
    - Service-defined custom query options
    - Improve behaviour/error message when receiving HTTP 204 (no content), e.g. from nonexistent ID
- Create (#4)
- Update (#4)
- Delete (#4)
- Add relationship (#4)
- Modify relationship (#4)
- Call action (#5)
- Call function (#5)
- ETag support (#29)
*/

pub mod http_client;

use self::http_client::ODataHttpClient;
use crate::{
    interactions::ODataInteraction,
    path::{AsStr, EntityPath},
    requests::delete_entity::DeleteEntity,
    EntityProperties,
};
use anyhow::{anyhow, Context};
use lazy_static::lazy_static;
use regex::Regex;
use reqwest::{
    header::{ACCEPT, CONTENT_TYPE},
    Method, Request, Url,
};
use serde::{ser::Error, Deserialize, Serialize};
use std::{future::Future, marker::PhantomData, pin::Pin};

/// Represents an Entity Set in an OData service.
#[derive(Debug)]
pub struct EntitySetEndpoint<P, S = &'static str> {
    pub service_url: S,
    pub name: S,
    pub marker: PhantomData<P>,
}

impl<P: EntityProperties + for<'de> Deserialize<'de> + Serialize, S: AsStr>
    EntitySetEndpoint<P, S>
{
    /// Retrieves entity records in this entity set.
    // TODO #3: `$filter` support
    pub async fn retrieve<Client: ODataHttpClient>(
        &self,
        client: &Client,
    ) -> Result<Vec<Entity<P>>, anyhow::Error> {
        retrieve_entity_set(client, self.service_url.as_ref(), self.name.as_ref()).await
    }

    /// Retrieves a single entity record from the entity set.
    pub async fn retrieve_entity<Client: ODataHttpClient>(
        &self,
        client: &Client,
        id: &str,
    ) -> Result<Entity<P>, anyhow::Error> {
        retrieve_entity_from_set(client, self.service_url.as_ref(), self.name.as_ref(), id).await
    }

    pub async fn create_entity<Client: ODataHttpClient>(
        &self,
        client: &Client,
        to_create: &P,
    ) -> Result<Entity<P>, anyhow::Error> {
        create_entity(
            client,
            self.service_url.as_ref(),
            self.name.as_ref(),
            to_create,
        )
        .await
    }

    pub async fn update_entity<Client: ODataHttpClient>(
        &self,
        client: &Client,
        to_update: &P,
    ) -> Result<(), anyhow::Error> {
        update_entity(client, to_update).await
    }
}

/// Represents a singleton in an OData service.
#[derive(Debug)]
pub struct SingletonEndpoint<P, S = &'static str> {
    pub service_url: S,
    pub name: S,
    pub marker: PhantomData<P>,
}

impl<P: EntityProperties + for<'de> Deserialize<'de>, S: AsStr> SingletonEndpoint<P, S> {
    /// Retrieves the entity record which occupies the singleton endpoint.
    pub async fn get<Client: ODataHttpClient>(
        &self,
        client: &Client,
    ) -> Result<Entity<P>, anyhow::Error> {
        retrieve_singleton_entity(client, self.service_url.as_ref(), self.name.as_ref()).await
    }
}

/// Represents a retrieved entity record.
#[derive(Debug, Deserialize)]
pub struct Entity<P> {
    #[serde(rename = "@odata.id")]
    pub id: Option<EntityPath<String>>,
    #[serde(rename = "@odata.etag")]
    pub etag: Option<String>,
    #[serde(rename = "@odata.editLink")]
    pub edit_link: Option<EntityPath<String>>,
    #[serde(flatten)]
    pub properties: P,
}

impl<P: EntityProperties> Entity<P> {
    pub fn to_entity_link(&self) -> Result<EntityLink<P>, anyhow::Error> {
        match &self.id {
            None => Err(anyhow!("Retrieved entity model missing `@odata.id` field")),
            Some(id) => Ok(EntityLink {
                entity_path: id.clone(),
                marker: PhantomData,
            }),
        }
    }
}

/// A link to a fetchable entity record.
#[derive(Debug, Deserialize)]
pub struct EntityLink<P> {
    #[serde(skip)]
    pub marker: PhantomData<P>,
    #[serde(rename = "@odata.id")]
    pub entity_path: EntityPath<String>,
}

/// Manual impl required because `PhantomData` imposes type constraint in derive
impl<P: EntityProperties> PartialEq for EntityLink<P> {
    fn eq(&self, other: &Self) -> bool {
        self.entity_path.eq(&other.entity_path)
    }
}

/// Manual impl required because `PhantomData` imposes type constraint in derive
impl<P: EntityProperties> Clone for EntityLink<P> {
    fn clone(&self) -> Self {
        EntityLink {
            marker: PhantomData,
            entity_path: self.entity_path.clone(),
        }
    }
}

impl<P> EntityLink<P> {
    /// Obtains the local suffix of the full entity link (e.g. `/entitySet(id)`)
    pub fn bind_path(&self) -> Result<&str, anyhow::Error> {
        lazy_static! {
            static ref BIND_PATH_PATTERN: Regex =
                Regex::new(r#"/[\d\w_-]+\('?[\d\w_-]+'?\)$"#).unwrap();
        }

        match BIND_PATH_PATTERN.find(&self.entity_path.0) {
            Some(path) => Ok(path.as_str()),
            None => Err(anyhow!(
                "entity link URL '{}' does not end in valid bind path",
                self.entity_path
            )),
        }
    }

    pub fn delete<'a, Client: ODataHttpClient>(
        &'a self,
        client: &'a Client,
    ) -> Pin<Box<dyn Future<Output = Result<(), anyhow::Error>> + 'a>>
    where
        P: 'a,
    {
        todo!()
        // Box::pin(async {
        //     DeleteEntity {
        //         entity_path: EntityPath::from_str(&self.entity_path).unwrap(),
        //     }
        //     .perform(client, "asdasd".to_string())
        //     .await
        // })
    }
}

impl<P: EntityProperties + for<'de> Deserialize<'de>> EntityLink<P> {
    /// Fetch this record in full.
    pub async fn get(&self, client: &impl ODataHttpClient) -> Result<Entity<P>, anyhow::Error> {
        let request = Request::new(
            Method::GET,
            with_expand_query::<P>(
                Url::parse(&self.entity_path.0).context("Entity link ID is not valid URL")?,
            ),
        );

        let response: ServiceResponse<Entity<P>> = execute_request(client, request).await?;

        Ok(response.payload)
    }
}

/// Serializes entity links as a bind path string, appropriate for usage in upsert `odata@bind`
/// fields
impl<P: EntityProperties> Serialize for EntityLink<P> {
    fn serialize<S>(&self, serializer: S) -> Result<S::Ok, S::Error>
    where
        S: serde::Serializer,
    {
        let bind_path = self.bind_path().map_err(S::Error::custom)?;
        serializer.serialize_str(bind_path)
    }
}

async fn retrieve_entity_set<
    P: EntityProperties + for<'de> Deserialize<'de>,
    Client: ODataHttpClient,
>(
    client: &Client,
    service_url: &str,
    set_name: &str,
) -> Result<Vec<Entity<P>>, anyhow::Error> {
    let mut entities = Vec::new();
    let mut next_page_url = with_expand_query::<P>(
        Url::parse(&format!("{}/{}", service_url, set_name)).expect("URL parse failed"),
    );

    loop {
        let request = Request::new(Method::GET, next_page_url);

        let mut response: ServiceResponse<EntitySetPayload<P>> =
            execute_request(client, request).await?;

        entities.append(&mut response.payload.value);

        if let Some(next_link) = response.payload.next_link {
            next_page_url = Url::parse(&next_link)?;
        } else {
            return Ok(entities);
        }
    }
}

async fn retrieve_entity_from_set<
    P: EntityProperties + for<'de> Deserialize<'de>,
    Client: ODataHttpClient,
>(
    client: &Client,
    service_url: &str,
    set_name: &str,
    id: &str,
) -> Result<Entity<P>, anyhow::Error> {
    let request = Request::new(
        Method::GET,
        with_expand_query::<P>(
            Url::parse(&format!("{}/{}('{}')", service_url, set_name, id))
                .expect("URL parse failed"),
        ),
    );

    let response: ServiceResponse<Entity<P>> = execute_request(client, request).await?;

    Ok(response.payload)
}

async fn retrieve_singleton_entity<
    P: EntityProperties + for<'de> Deserialize<'de>,
    Client: ODataHttpClient,
>(
    client: &Client,
    service_url: &str,
    singleton_name: &str,
) -> Result<Entity<P>, anyhow::Error> {
    let request = Request::new(
        Method::GET,
        with_expand_query::<P>(
            Url::parse(&format!("{}/{}", service_url, singleton_name)).expect("URL parse failed"),
        ),
    );

    let response: ServiceResponse<Entity<P>> = execute_request(client, request).await?;

    Ok(response.payload)
}

async fn create_entity<
    P: EntityProperties + for<'de> Deserialize<'de> + Serialize,
    Client: ODataHttpClient,
>(
    client: &Client,
    service_url: &str,
    set_name: &str,
    to_create: &P,
) -> Result<Entity<P>, anyhow::Error> {
    let payload = serde_json::to_string(&to_create)?;
    let mut request = Request::new(
        Method::POST,
        Url::parse(&format!("{}/{}", service_url, set_name)).expect("URL parse failed"),
    );

    add_content_modification_headers(&mut request);
    *request.body_mut() = Some(payload.into());

    let response: ServiceResponse<Entity<P>> = execute_request(client, request).await?;

    Ok(response.payload)
}

async fn delete_entity<P, Client: ODataHttpClient>(
    client: &Client,
    to_delete: &EntityLink<P>,
) -> Result<(), anyhow::Error> {
    let request = Request::new(Method::DELETE, Url::parse(&to_delete.entity_path.0)?);
    let resp: reqwest::Response = client.execute_request(request).await?.into();

    check_response_status(resp).await?;

    Ok(())
}

async fn update_entity<P: EntityProperties, Client: ODataHttpClient>(
    client: &Client,
    to_update: &P,
) -> Result<(), anyhow::Error> {
    todo!() // #4
}

fn with_expand_query<P: EntityProperties>(mut url: Url) -> Url {
    if let Some(query) = P::EXPAND_QUERY {
        url.query_pairs_mut().append_pair("$expand", query);
    }

    url
}

fn add_content_modification_headers(request: &mut Request) {
    request
        .headers_mut()
        .append(CONTENT_TYPE, "application/json".parse().unwrap());
    request
        .headers_mut()
        .append(ACCEPT, "application/json".parse().unwrap());
    request
        .headers_mut()
        .append("OData-Version", "4.0".parse().unwrap());
}

async fn execute_request<T: for<'de> Deserialize<'de>, Client: ODataHttpClient>(
    client: &Client,
    request: Request,
) -> Result<T, anyhow::Error> {
    let resp: reqwest::Response = client.execute_request(request).await?.into();
    let resp = check_response_status(resp).await?;
    let text = resp.text().await?;
    let deserialized = serde_json::from_str(&text)?;

    Ok(deserialized)
}

async fn check_response_status(
    resp: reqwest::Response,
) -> Result<reqwest::Response, anyhow::Error> {
    let status = resp.status();

    if status.is_success() {
        Ok(resp)
    } else {
        match resp.text().await {
            Ok(resp_body) => {
                return Err(anyhow!(
                    "Server response error {}, body: '{}'",
                    status.as_u16(),
                    resp_body
                ));
            }
            Err(_) => return Err(anyhow!("Server response error {}", status)),
        }
    }
}

#[derive(Debug, Deserialize)]
struct ServiceResponse<Payload> {
    #[serde(rename = "@odata.context")]
    context: String,
    #[serde(flatten)]
    payload: Payload,
}

#[derive(Debug, Deserialize)]
struct EntitySetPayload<P> {
    #[serde(rename = "@odata.nextLink")]
    next_link: Option<String>,
    value: Vec<Entity<P>>,
}
