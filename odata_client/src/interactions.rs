//! Definitions of common OData interactions which can be performed with a user-configurable HTTP
//! client, comprising one or more [ODataRequest]s.

pub mod retrieve_all;
pub mod retrieve_all_full;
pub mod retrieve_multiple_full;
pub mod retrieve_single_full;

use async_trait::async_trait;

// TODO #4 - Create as `ODataInteractions`:
// - create `EntityProperties` type including nav prop links
// - update `EntityProperties` type including nav prop links
// TODO #4: convert/update current `client` module

use crate::client::http_client::ODataHttpClient;
use crate::requests::ODataRequest;

#[async_trait]
pub trait ODataInteraction {
    type Output;

    async fn perform<C: ODataHttpClient>(&self, client: &C) -> Result<Self::Output, anyhow::Error>
    where
        for<'a> &'a C: Send;
}

#[async_trait]
impl<R> ODataInteraction for R
where
    R: ODataRequest,
    for<'a> &'a <R as ODataRequest>::Req: Send,
    for<'a> &'a R: Send,
{
    type Output = <Self as ODataRequest>::Resp;

    async fn perform<C: ODataHttpClient>(&self, client: &C) -> Result<Self::Output, anyhow::Error>
    where
        for<'a> &'a C: Send,
    {
        let req_url = reqwest::Url::parse(&self.url())?;
        let body: reqwest::Body = serde_json::to_string(self.body())?.into();
        let request = {
            let mut req = reqwest::Request::new(<Self as ODataRequest>::HTTP_METHOD, req_url);

            for &(name, val) in self.headers() {
                req.headers_mut().append(name, val.parse()?);
            }

            *req.body_mut() = Some(body);
            req
        };

        let resp: reqwest::Response = client.execute_request(request).await?.into();
        let text = resp.text().await?;
        let deserialized = serde_json::from_str(&text)?;

        Ok(deserialized)
    }
}
