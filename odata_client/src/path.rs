//! Models of OData request paths.

use anyhow::anyhow;
use odata_client_util::StrSplitOnce;
use serde::{Deserialize, Serialize};
use std::fmt;
use std::fmt::Display;

/// ```https://host.com/api/$metadata#EntitySet/$entity```
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(transparent)]
pub struct ContextUrl<S: AsStr>(pub S);

/// ```https://host.com/api/```
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(transparent)]
pub struct ServiceUrl<S: AsStr>(pub S);

/// `/set_name` or `service_path/set_name` or `[service_path]/.../set_name('key')/collection_prop_name`
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(transparent)]
pub struct EntitySetPath<S: AsStr>(pub S);

/// `[service_path]/.../set_name('key')`
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(transparent)]
pub struct EntityPath<S: AsStr>(pub S);

/// `[service_path]/.../set_name('key')/prop_name`
#[derive(Debug, Clone, Serialize, Deserialize, PartialEq)]
#[serde(transparent)]
pub struct EntityPropPath<S: AsStr>(pub S);

impl<S: AsStr> ContextUrl<S> {
    pub fn service_url(&self) -> Result<ServiceUrl<&str>, anyhow::Error> {
        match self.0.as_ref().rsplit_str_once("$metadata") {
            Some((service_url, _rest)) => Ok(ServiceUrl(service_url)),
            None => Err(anyhow!("Invalid context url '{}'", self.0.as_ref())),
        }
    }
}

macro_rules! impl_display {
    ($t:ident) => {
        impl<S: AsStr> Display for $t<S> {
            fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
                self.0.as_ref().fmt(f)
            }
        }
    };
}

impl_display!(ContextUrl);
impl_display!(ServiceUrl);
impl_display!(EntitySetPath);
impl_display!(EntityPath);
impl_display!(EntityPropPath);

macro_rules! impl_into_str {
    ($t:ident) => {
        impl<'a, S: AsStr> Into<$t<&'a str>> for &'a $t<S> {
            fn into(self) -> $t<&'a str> {
                $t(self.0.as_ref())
            }
        }
    };
}

impl_into_str!(ContextUrl);
impl_into_str!(ServiceUrl);
impl_into_str!(EntitySetPath);
impl_into_str!(EntityPath);
impl_into_str!(EntityPropPath);

/// Marker trait for `&str` and `String`
pub trait AsStr: AsRef<str> + Send + Sync + Serialize {}
impl<'a> AsStr for &'a str {}
impl AsStr for String {}
