//! "Atomic" OData interactions, comprising a single HTTP request & response.

pub mod create_entity;
pub mod create_entity_no_return;
pub mod create_link;
pub mod delete_entity;
pub mod delete_link;
pub mod retrieve_multiple;
pub mod retrieve_single;
pub mod update_entity;
pub mod update_entity_no_return;
pub mod update_link;

use serde::{Deserialize, Serialize};

/// Defines the form of a single HTTP OData request.
pub trait ODataRequest {
    /// Serializable type which represents the request body. Use `()` if request has no body.
    type Req: Serialize;

    /// Deserializable type which represents the expected response body. Use `()` if no body is
    /// expected.
    type Resp: for<'de> Deserialize<'de>;

    const HTTP_METHOD: http::Method;

    /// Required headers on the HTTP request.
    fn headers(&self) -> &[(&'static str, &str)];

    /// Writes the path & query string for the HTTP request. Does not include the base service
    /// endpoint URL.
    fn url(&self) -> String;

    fn body(&self) -> &Self::Req;
}
