//! Type definitions in the `TPrimitiveType` schema type, excluding "Collection(_)" variations.
//! Definition instances include applicable facet attributes.
use anyhow::anyhow;
use chrono::{DateTime, FixedOffset, NaiveDate, NaiveTime};
use iso8601::Duration;
use std::any::type_name;
use uuid::Uuid;

use crate::entity_data_model_parse::{edm, xs};

pub trait PrimitiveTypeDef: Sized {
    /// How a deserialised "DefaultValue" property attribute is encoded.
    type ValueRepresentation;

    /// Full module path to the `ValueRepreseentation` type
    fn value_representation_type_name() -> &'static str {
        type_name::<Self::ValueRepresentation>()
    }

    /// Optional path to type for `#[serde_as = "..."]` attribute, if required to deserialize this
    /// type
    fn deserialize_as_path() -> Option<&'static str> {
        None
    }

    /// How to parse a `default_value="..."` property attribute
    fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error>;

    /// Construct a type definition instance with the specified facet attributes. Should error if
    /// any attributes are missing, or extraneous attributes are specified.
    fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error>;
}

/// Generates `PrimitiveType` and `PrimitiveProperty` enums with variants for each primitive type
macro_rules! primitive_types {
    ($(
        $t_primitive_type:path, $t_primitive_type_collection:path => $name:ident {
            $decl:item
            $trait_impl:item
        }
    )*) => {
        $(
            $decl
            $trait_impl
        )*

        /// Representation of a primitive type instance without default value, used by primitive
        /// aliases (`TypeDefinition` items in entity model metadata)
        #[derive(Debug, Clone, PartialEq)]
        pub enum PrimitiveType {
            $(
                $name($name),
            )*
        }

        impl PrimitiveType {
            /// Returns constructed primitive type definition & bool indicating whether it represents a collection.
            pub fn from_property_attributes(
                t_primitive_type: edm::TPrimitiveType,
                facet_attributes: edm::TFacetAttributes,
            ) -> Result<(Self, bool), anyhow::Error> {
                match t_primitive_type {
                    $(
                        $t_primitive_type => {
                            Ok((PrimitiveType::$name($name::from_attributes(facet_attributes)?), false))
                        }
                        $t_primitive_type_collection => {
                            Ok((PrimitiveType::$name($name::from_attributes(facet_attributes)?), true))
                        },
                    )*
                }
            }

            pub fn value_representation_type_name(&self) -> &'static str {
                match self {
                    $(
                        PrimitiveType::$name(_) => <$name as PrimitiveTypeDef>::value_representation_type_name(),
                    )*
                }
            }

            pub fn deserialize_as_path(&self) -> Option<&'static str> {
                match self {
                    $(
                        PrimitiveType::$name(_) => <$name as PrimitiveTypeDef>::deserialize_as_path(),
                    )*
                }
            }
        }

        /// Representatin of a primitive type instance with optional default value, used by complex/
        /// entity type properties with primitive types
        #[derive(Debug, Clone, PartialEq)]
        pub enum PrimitiveProperty {
            $(
                $name {
                    attrs: $name,
                    default_value: Option<<$name as PrimitiveTypeDef>::ValueRepresentation>
                },
            )*
        }

        impl PrimitiveProperty {
            /// Returns constructed primitive property definition & bool indicating whether it represents a collection.
            pub fn from_property_attributes(
                t_primitive_type: edm::TPrimitiveType,
                facet_attributes: edm::TFacetAttributes,
                default_value_str: Option<String>
            ) -> Result<(Self, bool), anyhow::Error> {
                match t_primitive_type {
                    $(
                        $t_primitive_type | $t_primitive_type_collection => {
                            let is_collection = matches!(t_primitive_type, $t_primitive_type_collection);
                            let prim_prop = PrimitiveProperty::$name {
                                attrs: $name::from_attributes(facet_attributes)?,
                                default_value: match default_value_str {
                                    Some(s) => Some($name::parse_default_value(s)?),
                                    None => None
                                }
                            };

                            Ok((prim_prop, is_collection))
                        }
                    )*
                }

            }

            pub fn value_representation_type_name(&self) -> &'static str {
                match self {
                    $(
                        PrimitiveProperty::$name { .. } => <$name as PrimitiveTypeDef>::value_representation_type_name(),
                    )*
                }
            }

            pub fn deserialize_as_path(&self) -> Option<&'static str> {
                match self {
                    $(
                        PrimitiveProperty::$name { .. } => <$name as PrimitiveTypeDef>::deserialize_as_path(),
                    )*
                }
            }
        }
    };
}

use edm::TPrimitiveType::*;

primitive_types! {
    EdmBinary, CollectionEdmBinary => BinaryDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct BinaryDef {
            max_length: MaxLengthFacet,
        }

        impl PrimitiveTypeDef for BinaryDef {
            type ValueRepresentation = Vec<u8>;

            /// `typename` roots `Vec` in the `alloc` crate
            fn value_representation_type_name() -> &'static str {
                "Vec<u8>"
            }

            fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(base64::decode(ser)?)
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                match facet_attributes {
                    edm::TFacetAttributes {
                        max_length,
                        precision: None,
                        scale: None,
                        srid: None,
                        unicode: None
                    } => Ok(BinaryDef {
                        max_length: max_length.map(|m| m.into()).unwrap_or(MaxLengthFacet::Unlimited)
                    }),
                    attrs => Err(anyhow!("Invalid facet attributes for primitive type Binary {:?}", attrs))
                }
            }
        }
    }

    EdmBoolean, CollectionEdmBoolean => BooleanDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct BooleanDef;

        impl PrimitiveTypeDef for BooleanDef {
            type ValueRepresentation = bool;

            fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                match &ser[..] {
                    "1" | "true" => Ok(true),
                    "0" | "false" => Ok(true),
                    s => Err(anyhow!("Invalid boolean value {}", s))
                }
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                match facet_attributes {
                    edm::TFacetAttributes {
                        max_length: None,
                        precision: None,
                        scale: None,
                        srid: None,
                        unicode: None
                    } => Ok(BooleanDef),
                    attrs => Err(anyhow!("Invalid facet attributes for primitive type Boolean {:?}", attrs))
                }
            }
        }
    }

    EdmByte, CollectionEdmByte => ByteDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct ByteDef;

        impl PrimitiveTypeDef for ByteDef {
            type ValueRepresentation = u8;

            fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(ser.parse()?)
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                match facet_attributes {
                    edm::TFacetAttributes {
                        max_length: None,
                        precision: None,
                        scale: None,
                        srid: None,
                        unicode: None
                    } => Ok(ByteDef),
                    attrs => Err(anyhow!("Invalid facet attributes for primitive type Byte {:?}", attrs))
                }
            }
        }
    }

    EdmDate, CollectionEdmDate => DateDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct DateDef;

        impl PrimitiveTypeDef for DateDef {
            type ValueRepresentation = NaiveDate;

            fn value_representation_type_name() -> &'static str {
                "NaiveDate"
            }

            fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(NaiveDate::parse_from_str(&ser, "%Y-%m-%d")?)
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                match facet_attributes {
                    edm::TFacetAttributes {
                        max_length: None,
                        precision: None,
                        scale: None,
                        srid: None,
                        unicode: None
                    } => Ok(DateDef),
                    attrs => Err(anyhow!("Invalid facet attributes for primitive type Date {:?}", attrs))
                }
            }
        }
    }

    EdmDateTimeOffset, CollectionEdmDateTimeOffset => DateTimeOffsetDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct DateTimeOffsetDef(TemporalDef);

        impl PrimitiveTypeDef for DateTimeOffsetDef {
            type ValueRepresentation = DateTime<FixedOffset>;

            /// The path produced by default impl is a private declaration (re-exported at different
            /// path)
            fn value_representation_type_name() -> &'static str {
                "DateTime<FixedOffset>"
            }

            fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(DateTime::parse_from_rfc2822(&ser)?)
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(DateTimeOffsetDef(TemporalDef::from_facet_attrs(facet_attributes)?))
            }
        }
    }

    EdmDecimal, CollectionEdmDecimal => DecimalDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct DecimalDef {
            precision: DecimalPrecisionFacet,
            scale: ScaleFacet,
        }

        impl PrimitiveTypeDef for DecimalDef {
            type ValueRepresentation = (i64, u64);

            fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                let mut split = ser.split('.');
                match (split.next(), split.next(), split.next()) {
                    (Some(lhs), None, None) =>  {
                        Ok((lhs.parse()?, 0))
                    }
                    (Some(lhs), Some(rhs), None) => {
                        Ok((lhs.parse()?, rhs.parse()?))
                    }
                    _ => Err(anyhow!("Invalid decimal value '{}'", ser))
                }
            }

            fn from_attributes(attrs: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                match attrs {
                    edm::TFacetAttributes {
                        max_length: None,
                        precision,
                        scale,
                        srid: None,
                        unicode: None,
                    } =>{
                        let precision = match precision {
                            Some(p) => DecimalPrecisionFacet::Fixed(p.0.0),
                            None => DecimalPrecisionFacet::Arbitrary,
                        };
                        let scale: ScaleFacet = match scale {
                            Some(scale) => scale.into(),
                            // odata-csdl-xml-v4.01 sec 7.2.4
                            None => ScaleFacet::MaxDecimalDigits(0),
                        };

                        if let ScaleFacet::MaxDecimalDigits(sd) = scale {
                            // odata-csdl-xml-v4.01 sec 7.2.4
                            if let DecimalPrecisionFacet::Fixed(p) = precision {
                                if sd > p {
                                    return Err(anyhow!("Decimal scale ({}) should not be greater than precision ({:?})", sd, precision));
                                }
                            }
                        }

                        Ok(DecimalDef { precision, scale })
                    } ,
                    attrs => Err(anyhow!("Invalid facet attributes for primitive type Decimal {:?}", attrs)),
                }
            }
        }
    }

    EdmDouble, CollectionEdmDouble => DoubleDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct DoubleDef;

        impl PrimitiveTypeDef for DoubleDef {
            type ValueRepresentation = f64;

            fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(ser.parse()?)
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                match facet_attributes {
                    edm::TFacetAttributes {
                        max_length: None,
                        precision: None,
                        scale: None,
                        srid: None,
                        unicode: None
                    } => Ok(DoubleDef),
                    attrs => Err(anyhow!("Invalid facet attributes for primitive type Double {:?}", attrs))
                }
            }
        }
    }

    EdmDuration, CollectionEdmDuration => DurationDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct DurationDef(TemporalDef);

        impl PrimitiveTypeDef for DurationDef {
            /// Specified values for each component (years, months etc.) of the ISO8601 duration are
            /// recorded, rather than converting to a precise `chrono::Duration`, because the exact
            /// length of time cannot be unambiguously calculated.
            type ValueRepresentation = Duration;

            fn deserialize_as_path() -> Option<&'static str> {
                Some("deserialize_with::Iso8601DurationDeserialize")
            }

            fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                iso8601::duration(&ser).map_err(|msg| anyhow!(msg))
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(DurationDef(TemporalDef::from_facet_attrs(facet_attributes)?))
            }
        }
    }

    EdmGuid, CollectionEdmGuid => GuidDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct GuidDef;

        impl PrimitiveTypeDef for GuidDef {
            type ValueRepresentation = Uuid;

            fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(Uuid::parse_str(&ser)?)
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                match facet_attributes {
                    edm::TFacetAttributes {
                        max_length: None,
                        precision: None,
                        scale: None,
                        srid: None,
                        unicode: None
                    } => Ok(GuidDef),
                    attrs => Err(anyhow!("Invalid facet attributes for primitive type Guid {:?}", attrs))
                }
            }
        }
    }

    EdmInt16, CollectionEdmInt16 => Int16Def {
        #[derive(Debug, Clone, PartialEq)]
        pub struct Int16Def;

        impl PrimitiveTypeDef for Int16Def {
            type ValueRepresentation = i16;

            fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(ser.parse()?)
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                match facet_attributes {
                    edm::TFacetAttributes {
                        max_length: None,
                        precision: None,
                        scale: None,
                        srid: None,
                        unicode: None
                    } => Ok(Int16Def),
                    attrs => Err(anyhow!("Invalid facet attributes for primitive type Int16 {:?}", attrs))
                }
            }
        }
    }

    EdmInt32, CollectionEdmInt32 => Int32Def {
        #[derive(Debug, Clone, PartialEq)]
        pub struct Int32Def;

        impl PrimitiveTypeDef for Int32Def {
            type ValueRepresentation = i32;

            fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(ser.parse()?)
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                match facet_attributes {
                    edm::TFacetAttributes {
                        max_length: None,
                        precision: None,
                        scale: None,
                        srid: None,
                        unicode: None
                    } => Ok(Int32Def),
                    attrs => Err(anyhow!("Invalid facet attributes for primitive type Int32 {:?}", attrs))
                }
            }
        }
    }

    EdmInt64, CollectionEdmInt64 => Int64Def {
        #[derive(Debug, Clone, PartialEq)]
        pub struct Int64Def;

        impl PrimitiveTypeDef for Int64Def {
            type ValueRepresentation = i64;

            fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(ser.parse()?)
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                match facet_attributes {
                    edm::TFacetAttributes {
                        max_length: None,
                        precision: None,
                        scale: None,
                        srid: None,
                        unicode: None
                    } => Ok(Int64Def),
                    attrs => Err(anyhow!("Invalid facet attributes for primitive type Int64 {:?}", attrs))
                }
            }
        }
    }

    EdmSByte, CollectionEdmSByte => SByteDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct SByteDef;

        impl PrimitiveTypeDef for SByteDef {
            type ValueRepresentation = i8;

            fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(ser.parse()?)
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                match facet_attributes {
                    edm::TFacetAttributes {
                        max_length: None,
                        precision: None,
                        scale: None,
                        srid: None,
                        unicode: None
                    } => Ok(SByteDef),
                    attrs => Err(anyhow!("Invalid facet attributes for primitive type SByte {:?}", attrs))
                }
            }
        }
    }

    EdmSingle, CollectionEdmSingle => SingleDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct SingleDef;

        impl PrimitiveTypeDef for SingleDef {
            type ValueRepresentation = f32;

            fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(ser.parse()?)
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                match facet_attributes {
                    edm::TFacetAttributes {
                        max_length: None,
                        precision: None,
                        scale: None,
                        srid: None,
                        unicode: None
                    } => Ok(SingleDef),
                    attrs => Err(anyhow!("Invalid facet attributes for primitive type Single {:?}", attrs))
                }
            }
        }
    }

    EdmString, CollectionEdmString => StringDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct StringDef {
            max_length: MaxLengthFacet,
            unicode: bool,
        }

        impl PrimitiveTypeDef for StringDef {
            type ValueRepresentation = String;

            /// `typename` roots `String` in the `alloc` crate
            fn value_representation_type_name() -> &'static str {
                "String"
            }

            fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(ser)
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                match facet_attributes {
                    edm::TFacetAttributes {
                        max_length,
                        precision: None,
                        scale: None,
                        srid: None,
                        unicode
                    } => Ok(StringDef {
                        max_length: max_length.map(|m| m.into()).unwrap_or(MaxLengthFacet::Unlimited),
                        unicode: unicode.map(|u| u.0.0).unwrap_or(true)
                    }),
                    attrs => Err(anyhow!("Invalid facet attributes for primitive type String {:?}", attrs))
                }
            }
        }
    }

    EdmTimeOfDay, CollectionEdmTimeOfDay => TimeOfDayDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct TimeOfDayDef(TemporalDef);

        impl PrimitiveTypeDef for TimeOfDayDef {
            type ValueRepresentation = NaiveTime;

            fn value_representation_type_name() -> &'static str {
                "NaiveTime"
            }

            fn parse_default_value(ser: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(
                    NaiveTime::parse_from_str(&ser, "%H:%M:%S")
                        .or_else(|_| NaiveTime::parse_from_str(&ser, "%H:%M:%S.%f"))?
                )
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(TimeOfDayDef(TemporalDef::from_facet_attrs(facet_attributes)?))
            }
        }
    }

    // TODO #25: Value representations for geo types
    EdmGeographyPoint, CollectionEdmGeographyPoint => GeographyPointDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct GeographyPointDef(GeoDef);

        impl PrimitiveTypeDef for GeographyPointDef {
            type ValueRepresentation = ();

            fn parse_default_value(_: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(())
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(GeographyPointDef(GeoDef::from_facet_attrs(facet_attributes, DEFAULT_GEOGRAPHY_SRID)?))
            }
        }
    }

    EdmGeographyLineString, CollectionEdmGeographyLineString => GeographyLineStringDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct GeographyLineStringDef(GeoDef);

        impl PrimitiveTypeDef for GeographyLineStringDef {
            type ValueRepresentation = ();

            fn parse_default_value(_: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(())
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(GeographyLineStringDef(GeoDef::from_facet_attrs(facet_attributes, DEFAULT_GEOGRAPHY_SRID)?))
            }
        }
    }

    EdmGeographyPolygon, CollectionEdmGeographyPolygon => GeographyPolygonDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct GeographyPolygonDef(GeoDef);

        impl PrimitiveTypeDef for GeographyPolygonDef {
            type ValueRepresentation = ();

            fn parse_default_value(_: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(())
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(GeographyPolygonDef(GeoDef::from_facet_attrs(facet_attributes, DEFAULT_GEOGRAPHY_SRID)?))
            }
        }
    }

    EdmGeographyMultiPoint, CollectionEdmGeographyMultiPoint => GeographyMultiPointDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct GeographyMultiPointDef(GeoDef);

        impl PrimitiveTypeDef for GeographyMultiPointDef {
            type ValueRepresentation = ();

            fn parse_default_value(_: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(())
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(GeographyMultiPointDef(GeoDef::from_facet_attrs(facet_attributes, DEFAULT_GEOGRAPHY_SRID)?))
            }
        }
    }

    EdmGeographyMultiLineString, CollectionEdmGeographyMultiLineString => GeographyMultiLineStringDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct GeographyMultiLineStringDef(GeoDef);

        impl PrimitiveTypeDef for GeographyMultiLineStringDef {
            type ValueRepresentation = ();

            fn parse_default_value(_: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(())
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(GeographyMultiLineStringDef(GeoDef::from_facet_attrs(facet_attributes, DEFAULT_GEOGRAPHY_SRID)?))
            }
        }
    }

    EdmGeographyMultiPolygon, CollectionEdmGeographyMultiPolygon => GeographyMultiPolygonDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct GeographyMultiPolygonDef(GeoDef);

        impl PrimitiveTypeDef for GeographyMultiPolygonDef {
            type ValueRepresentation = ();

            fn parse_default_value(_: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(())
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(GeographyMultiPolygonDef(GeoDef::from_facet_attrs(facet_attributes, DEFAULT_GEOGRAPHY_SRID)?))
            }
        }
    }

    EdmGeographyCollection, CollectionEdmGeographyCollection => GeographyCollectionDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct GeographyCollectionDef(GeoDef);

        impl PrimitiveTypeDef for GeographyCollectionDef {
            type ValueRepresentation = ();

            fn parse_default_value(_: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(())
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(GeographyCollectionDef(GeoDef::from_facet_attrs(facet_attributes, DEFAULT_GEOGRAPHY_SRID)?))
            }
        }
    }

    EdmGeometryPoint, CollectionEdmGeometryPoint => GeometryPointDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct GeometryPointDef(GeoDef);

        impl PrimitiveTypeDef for GeometryPointDef {
            type ValueRepresentation = ();

            fn parse_default_value(_: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(())
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(GeometryPointDef(GeoDef::from_facet_attrs(facet_attributes, DEFAULT_GEOMETRY_SRID)?))
            }
        }
    }

    EdmGeometryLineString, CollectionEdmGeometryLineString => GeometryLineStringDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct GeometryLineStringDef(GeoDef);

        impl PrimitiveTypeDef for GeometryLineStringDef {
            type ValueRepresentation = ();

            fn parse_default_value(_: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(())
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(GeometryLineStringDef(GeoDef::from_facet_attrs(facet_attributes, DEFAULT_GEOMETRY_SRID)?))
            }
        }
    }

    EdmGeometryPolygon, CollectionEdmGeometryPolygon => GeometryPolygonDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct GeometryPolygonDef(GeoDef);

        impl PrimitiveTypeDef for GeometryPolygonDef {
            type ValueRepresentation = ();

            fn parse_default_value(_: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(())
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(GeometryPolygonDef(GeoDef::from_facet_attrs(facet_attributes, DEFAULT_GEOMETRY_SRID)?))
            }
        }
    }

    EdmGeometryMultiPoint, CollectionEdmGeometryMultiPoint => GeometryMultiPointDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct GeometryMultiPointDef(GeoDef);

        impl PrimitiveTypeDef for GeometryMultiPointDef {
            type ValueRepresentation = ();

            fn parse_default_value(_: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(())
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(GeometryMultiPointDef(GeoDef::from_facet_attrs(facet_attributes, DEFAULT_GEOMETRY_SRID)?))
            }
        }
    }

    EdmGeometryMultiLineString, CollectionEdmGeometryMultiLineString => GeometryMultiLineStringDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct GeometryMultiLineStringDef(GeoDef);

        impl PrimitiveTypeDef for GeometryMultiLineStringDef {
            type ValueRepresentation = ();

            fn parse_default_value(_: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(())
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(GeometryMultiLineStringDef(GeoDef::from_facet_attrs(facet_attributes, DEFAULT_GEOMETRY_SRID)?))
            }
        }
    }

    EdmGeometryMultiPolygon, CollectionEdmGeometryMultiPolygon => GeometryMultiPolygonDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct GeometryMultiPolygonDef(GeoDef);

        impl PrimitiveTypeDef for GeometryMultiPolygonDef {
            type ValueRepresentation = ();

            fn parse_default_value(_: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(())
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(GeometryMultiPolygonDef(GeoDef::from_facet_attrs(facet_attributes, DEFAULT_GEOMETRY_SRID)?))
            }
        }
    }

    EdmGeometryCollection, CollectionEdmGeometryCollection => GeometryCollectionDef {
        #[derive(Debug, Clone, PartialEq)]
        pub struct GeometryCollectionDef(GeoDef);

        impl PrimitiveTypeDef for GeometryCollectionDef {
            type ValueRepresentation = ();

            fn parse_default_value(_: String) -> Result<Self::ValueRepresentation, anyhow::Error> {
                Ok(())
            }

            fn from_attributes(facet_attributes: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
                Ok(GeometryCollectionDef(GeoDef::from_facet_attrs(facet_attributes, DEFAULT_GEOMETRY_SRID)?))
            }
        }
    }
}

// odata-csdl-xml-v4.01 sec 7.2.6
const DEFAULT_GEOMETRY_SRID: u64 = 0;
const DEFAULT_GEOGRAPHY_SRID: u64 = 4326;

#[derive(Debug, Clone, PartialEq)]
pub struct GeoDef {
    srid: SridFacet,
}

impl GeoDef {
    fn from_facet_attrs(
        attrs: edm::TFacetAttributes,
        default_srid: u64,
    ) -> Result<Self, anyhow::Error> {
        match attrs {
            edm::TFacetAttributes {
                max_length: None,
                precision: None,
                scale: None,
                srid,
                unicode: None,
            } => Ok(GeoDef {
                srid: srid
                    .map(|s| s.into())
                    .unwrap_or(SridFacet::Id(default_srid)),
            }),
            attrs => Err(anyhow!(
                "Invalid facet attributes for Geometry/Geography primitive type {:?}",
                attrs
            )),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum DecimalPrecisionFacet {
    Arbitrary,
    Fixed(u64),
}

#[derive(Debug, Clone, PartialEq)]
pub enum ScaleFacet {
    Floating,
    Variable,
    MaxDecimalDigits(u64),
}

impl From<edm::TScaleFacet> for ScaleFacet {
    fn from(t_scale_facet: edm::TScaleFacet) -> Self {
        match t_scale_facet {
            edm::TScaleFacet::Floating(edm::TFloating::Floating) => ScaleFacet::Floating,
            edm::TScaleFacet::Variable(edm::TVariable::Variable) => ScaleFacet::Variable,
            edm::TScaleFacet::NonNegativeInteger(digits) => ScaleFacet::MaxDecimalDigits(digits.0),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum MaxLengthFacet {
    Unlimited,
    MaxOf(u64),
}

impl From<edm::TMaxLengthFacet> for MaxLengthFacet {
    fn from(t_max_length_facet: edm::TMaxLengthFacet) -> Self {
        match t_max_length_facet {
            edm::TMaxLengthFacet::Max(edm::TMax::Max) => MaxLengthFacet::Unlimited,
            edm::TMaxLengthFacet::NonNegativeInteger(max) => MaxLengthFacet::MaxOf(max.0),
        }
    }
}

#[derive(Debug, Clone, PartialEq)]
pub enum SridFacet {
    Id(u64),
    Variable,
}

impl From<edm::TSridFacet> for SridFacet {
    fn from(t_srid_facet: edm::TSridFacet) -> Self {
        match t_srid_facet {
            edm::TSridFacet::Variable(edm::TVariable::Variable) => SridFacet::Variable,
            edm::TSridFacet::NonNegativeInteger(n) => SridFacet::Id(n.0),
        }
    }
}

// TODO #26: use precision value when serializing data to send to server
#[derive(Debug, Clone, PartialEq)]
pub struct TemporalDef {
    precision: u64,
}

impl TemporalDef {
    fn from_facet_attrs(attrs: edm::TFacetAttributes) -> Result<Self, anyhow::Error> {
        match attrs {
            edm::TFacetAttributes {
                max_length: None,
                precision,
                scale: None,
                srid: None,
                unicode: None,
            } => {
                // odata-csdl-xml-v4.01 sec 7.2.3
                let precision = match precision {
                    Some(edm::TPrecisionFacet(xs::NonNegativeInteger(p))) => {
                        if p > 12 {
                            return Err(anyhow!(
                                "Precision for temporal type must be 0..=12, found {}",
                                p
                            ));
                        } else {
                            p
                        }
                    }
                    None => 0,
                };

                Ok(TemporalDef { precision })
            }
            attrs => Err(anyhow!(
                "Invalid facet attributes for temporal primitive type {:?}",
                attrs
            )),
        }
    }
}
