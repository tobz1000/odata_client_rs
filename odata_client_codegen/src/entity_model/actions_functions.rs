use std::rc::Rc;

use super::{Property, PropertyVariant};

// TODO #5

pub struct Action {
    name: String,
    parameters: Vec<Property>,
    return_type: Option<ActionFunctionReturnType>,
    // entity_set_path: Option<???>,
    is_bound: bool,
}

pub struct Function {
    name: String,
    parameters: Vec<Property>,
    return_type: Option<ActionFunctionReturnType>,
    // entity_set_path: Option<???>,
    is_bound: bool,
    is_composable: bool,
}

pub struct ActionFunctionReturnType {
    r#type: PropertyVariant,
    nullable: bool,
    is_collection: bool,
}

pub struct Term {
    name: String,
    r#type: PropertyVariant,
    nullable: bool,
    is_collection: bool,
    base_term: Option<Rc<Term>>,
    // applies_to: AppliesTo,
}
