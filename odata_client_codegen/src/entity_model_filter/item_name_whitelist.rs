use std::collections::HashSet;

use crate::entity_model::{
    definition_lookup::NamedEdmItem, ComplexType, EntitySet, EntityType, EnumType,
    PrimitiveTypeAlias, Singleton,
};

use super::EntityModelFilter;

#[derive(Debug, Default, Clone)]
pub struct ItemNameWhitelist<'a> {
    entity_sets: HashSet<&'a str>,
    singletons: HashSet<&'a str>,
    entity_types: HashSet<&'a str>,
    complex_types: HashSet<&'a str>,
    primitive_aliases: HashSet<&'a str>,
    enum_types: HashSet<&'a str>,
}

macro_rules! whitelist_with_fn {
    ($fn_name:ident, $whitelist_field:ident) => {
        pub fn $fn_name(mut self, names: impl IntoIterator<Item = impl Into<&'a str>>) -> Self {
            for name in names {
                self.$whitelist_field.insert(name.into());
            }

            self
        }
    };
}

/// Specifies which Entity Data Model items to include by supplying
/// lists of names of items to be included. Used in code generation functions, e.g.
/// [`write_module_build_artifact`](crate::write_module_build_artifact)).
///
/// ```
/// use odata_client_codegen::ItemNameWhitelist;
///
/// // Specifies that only entity sets, singletons and enum types with these names - and their
/// // dependencies - should be included in a generated code module.
/// let mut item_filter = ItemNameWhitelist::new()
///     .with_entity_sets(vec!["Foo"])
///     .with_singletons(vec!["Bar", "Baz"])
///     .with_enum_types(vec!["Qux"]);
/// ```
///
/// See [`EntityModelFilter`] for more information about selecting code generation items.
// TODO #8: allow specifying by full qualified name/path
impl<'a> ItemNameWhitelist<'a> {
    pub fn new() -> Self {
        ItemNameWhitelist::default()
    }

    whitelist_with_fn!(with_entity_sets, entity_sets);
    whitelist_with_fn!(with_singletons, singletons);
    whitelist_with_fn!(with_entity_types, entity_types);
    whitelist_with_fn!(with_complex_types, complex_types);
    whitelist_with_fn!(with_primitive_aliases, primitive_aliases);
    whitelist_with_fn!(with_enum_types, enum_types);
}

impl<'a> EntityModelFilter for ItemNameWhitelist<'a> {
    fn include_entity_set(&mut self, entity_set: &EntitySet<'_>) -> bool {
        self.entity_sets.contains(&entity_set.name().path)
    }

    fn include_singleton(&mut self, singleton: &Singleton<'_>) -> bool {
        self.singletons.contains(&singleton.name().path)
    }

    fn include_entity_type(&mut self, entity_type: &EntityType<'_>) -> bool {
        self.entity_types.contains(&entity_type.name().uq_name)
    }

    fn include_complex_type(&mut self, complex_type: &ComplexType<'_>) -> bool {
        self.complex_types.contains(&complex_type.name().uq_name)
    }

    fn include_primitive_alias(&mut self, primitive_alias: &PrimitiveTypeAlias<'_>) -> bool {
        self.primitive_aliases
            .contains(&primitive_alias.name().uq_name)
    }

    fn include_enum_type(&mut self, enum_type: &EnumType<'_>) -> bool {
        self.enum_types.contains(&enum_type.name().uq_name)
    }
}
