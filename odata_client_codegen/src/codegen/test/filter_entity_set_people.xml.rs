#[allow(unused_imports)]
use odata_client::{
    chrono::{DateTime, FixedOffset, NaiveDate, NaiveTime},
    deserialize_with, iso8601,
    serde::{Deserialize, Serialize},
    serde_with::serde_as,
    uuid, EntityProperties, EntitySetEndpoint, ExpandQuery, FullEntityLink, SingletonEndpoint,
    StubEntityLink,
};
use std::marker::PhantomData;
pub const SERVICE_URL: &'static str = "http://services.odata.org/V4/TripPinService";
#[allow(dead_code)]
pub const PEOPLE: EntitySetEndpoint<Person> = EntitySetEndpoint {
    service_url: SERVICE_URL,
    name: "People",
    marker: PhantomData,
};
#[serde_as(crate = "odata_client::serde_with")]
#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "odata_client::serde")]
pub struct City {
    #[serde(rename = "CountryRegion")]
    pub country_region: String,
    #[serde(rename = "Name")]
    pub name: String,
    #[serde(rename = "Region")]
    pub region: String,
}
#[serde_as(crate = "odata_client::serde_with")]
#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "odata_client::serde")]
pub struct Location {
    #[serde(rename = "Address")]
    pub address: String,
    #[serde(rename = "City")]
    pub city: City,
}
#[derive(Debug)]
pub struct Photo;
#[serde_as(crate = "odata_client::serde_with")]
#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "odata_client::serde")]
pub struct Person {
    #[serde(rename = "UserName")]
    pub user_name: String,
    #[serde(rename = "FirstName")]
    pub first_name: String,
    #[serde(rename = "LastName")]
    pub last_name: String,
    #[serde(rename = "Emails")]
    pub emails: Vec<Option<String>>,
    #[serde(rename = "AddressInfo")]
    pub address_info: Vec<Option<Location>>,
    #[serde(rename = "Gender")]
    pub gender: Option<PersonGender>,
    #[serde(rename = "Concurrency")]
    pub concurrency: i64,
    #[serde(rename(deserialize = "Friends"))]
    #[serde(rename(serialize = "Friends@odata.bind"))]
    pub friends: Vec<FullEntityLink<Person>>,
    #[serde(rename = "Trips")]
    pub trips: Vec<Trip>,
    #[serde(rename(deserialize = "Photo"))]
    #[serde(rename(serialize = "Photo@odata.bind"))]
    pub photo: Option<StubEntityLink<Photo>>,
}
impl EntityProperties for Person {
    const EXPAND_QUERY: ExpandQuery =
        ExpandQuery::Expand("Friends/$ref,Trips($expand=Photos/$ref,PlanItems),Photo/$ref");
}
#[serde_as(crate = "odata_client::serde_with")]
#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "odata_client::serde")]
pub struct PlanItem {
    #[serde(rename = "PlanItemId")]
    pub plan_item_id: i32,
    #[serde(rename = "ConfirmationCode")]
    pub confirmation_code: Option<String>,
    #[serde(rename = "StartsAt")]
    pub starts_at: Option<DateTime<FixedOffset>>,
    #[serde(rename = "EndsAt")]
    pub ends_at: Option<DateTime<FixedOffset>>,
    #[serde(rename = "Duration")]
    #[serde_as(as = "Option<deserialize_with::Iso8601DurationDeserialize>")]
    pub duration: Option<iso8601::Duration>,
}
impl EntityProperties for PlanItem {
    const EXPAND_QUERY: ExpandQuery = ExpandQuery::None;
}
#[serde_as(crate = "odata_client::serde_with")]
#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "odata_client::serde")]
pub struct Trip {
    #[serde(rename = "TripId")]
    pub trip_id: i32,
    #[serde(rename = "ShareId")]
    pub share_id: Option<uuid::Uuid>,
    #[serde(rename = "Description")]
    pub description: Option<String>,
    #[serde(rename = "Name")]
    pub name: String,
    #[serde(rename = "Budget")]
    pub budget: f32,
    #[serde(rename = "StartsAt")]
    pub starts_at: DateTime<FixedOffset>,
    #[serde(rename = "EndsAt")]
    pub ends_at: DateTime<FixedOffset>,
    #[serde(rename = "Tags")]
    pub tags: Vec<String>,
    #[serde(rename(deserialize = "Photos"))]
    #[serde(rename(serialize = "Photos@odata.bind"))]
    pub photos: Vec<StubEntityLink<Photo>>,
    #[serde(rename = "PlanItems")]
    pub plan_items: Vec<PlanItem>,
}
impl EntityProperties for Trip {
    const EXPAND_QUERY: ExpandQuery = ExpandQuery::Expand("Photos/$ref,PlanItems");
}
#[serde_as(crate = "odata_client::serde_with")]
#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "odata_client::serde")]
pub enum PersonGender {
    Male = 0,
    Female = 1,
    Unknown = 2,
}
