pub const SERVICE_URL: &'static str = "http://services.odata.org/V4/TripPinService";
pub mod name_space_collision {
    use super::SERVICE_URL;
    #[allow(unused_imports)]
    use odata_client::{
        chrono::{DateTime, FixedOffset, NaiveDate, NaiveTime},
        deserialize_with, iso8601,
        serde::{Deserialize, Serialize},
        serde_with::serde_as,
        uuid, EntityProperties, EntitySetEndpoint, ExpandQuery, FullEntityLink, SingletonEndpoint,
        StubEntityLink,
    };
    use std::marker::PhantomData;
    #[serde_as(crate = "odata_client::serde_with")]
    #[derive(Debug, Deserialize, Serialize)]
    #[serde(crate = "odata_client::serde")]
    pub struct Type1 {}
    impl EntityProperties for Type1 {
        const EXPAND_QUERY: ExpandQuery = ExpandQuery::None;
    }
    #[serde_as(crate = "odata_client::serde_with")]
    #[derive(Debug, Deserialize, Serialize)]
    #[serde(crate = "odata_client::serde")]
    pub struct Type2 {}
    impl EntityProperties for Type2 {
        const EXPAND_QUERY: ExpandQuery = ExpandQuery::None;
    }
}
pub mod namespace_collision {
    use super::SERVICE_URL;
    #[allow(unused_imports)]
    use odata_client::{
        chrono::{DateTime, FixedOffset, NaiveDate, NaiveTime},
        deserialize_with, iso8601,
        serde::{Deserialize, Serialize},
        serde_with::serde_as,
        uuid, EntityProperties, EntitySetEndpoint, ExpandQuery, FullEntityLink, SingletonEndpoint,
        StubEntityLink,
    };
    use std::marker::PhantomData;
    #[serde_as(crate = "odata_client::serde_with")]
    #[derive(Debug, Deserialize, Serialize)]
    #[serde(crate = "odata_client::serde")]
    pub struct Type3 {}
    impl EntityProperties for Type3 {
        const EXPAND_QUERY: ExpandQuery = ExpandQuery::None;
    }
}
