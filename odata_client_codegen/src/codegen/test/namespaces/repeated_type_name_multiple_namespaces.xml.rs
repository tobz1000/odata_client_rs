pub const SERVICE_URL: &'static str = "http://services.odata.org/V4/TripPinService";
pub mod namespace_one {
    use super::SERVICE_URL;
    #[allow(unused_imports)]
    use odata_client::{
        chrono::{DateTime, FixedOffset, NaiveDate, NaiveTime},
        deserialize_with, iso8601,
        serde::{Deserialize, Serialize},
        serde_with::serde_as,
        uuid, EntityProperties, EntitySetEndpoint, ExpandQuery, FullEntityLink, SingletonEndpoint,
        StubEntityLink,
    };
    use std::marker::PhantomData;
    #[serde_as(crate = "odata_client::serde_with")]
    #[derive(Debug, Deserialize, Serialize)]
    #[serde(crate = "odata_client::serde")]
    pub struct SomeType {}
}
pub mod namespace_two {
    use super::SERVICE_URL;
    #[allow(unused_imports)]
    use odata_client::{
        chrono::{DateTime, FixedOffset, NaiveDate, NaiveTime},
        deserialize_with, iso8601,
        serde::{Deserialize, Serialize},
        serde_with::serde_as,
        uuid, EntityProperties, EntitySetEndpoint, ExpandQuery, FullEntityLink, SingletonEndpoint,
        StubEntityLink,
    };
    use std::marker::PhantomData;
    #[serde_as(crate = "odata_client::serde_with")]
    #[derive(Debug, Deserialize, Serialize)]
    #[serde(crate = "odata_client::serde")]
    pub struct SomeType {}
}
