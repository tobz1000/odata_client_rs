pub const SERVICE_URL: &'static str = "http://services.odata.org/V4/TripPinService";
pub mod name {
    pub mod space {
        pub mod one {
            use super::super::super::SERVICE_URL;
            #[allow(unused_imports)]
            use odata_client::{
                chrono::{DateTime, FixedOffset, NaiveDate, NaiveTime},
                deserialize_with, iso8601,
                serde::{Deserialize, Serialize},
                serde_with::serde_as,
                uuid, EntityProperties, EntitySetEndpoint, ExpandQuery, FullEntityLink,
                SingletonEndpoint, StubEntityLink,
            };
            use std::marker::PhantomData;
            #[serde_as(crate = "odata_client::serde_with")]
            #[derive(Debug, Deserialize, Serialize)]
            #[serde(crate = "odata_client::serde")]
            pub struct TypeOne {}
        }
        pub mod two {
            use super::super::super::SERVICE_URL;
            #[allow(unused_imports)]
            use odata_client::{
                chrono::{DateTime, FixedOffset, NaiveDate, NaiveTime},
                deserialize_with, iso8601,
                serde::{Deserialize, Serialize},
                serde_with::serde_as,
                uuid, EntityProperties, EntitySetEndpoint, ExpandQuery, FullEntityLink,
                SingletonEndpoint, StubEntityLink,
            };
            use std::marker::PhantomData;
            #[serde_as(crate = "odata_client::serde_with")]
            #[derive(Debug, Deserialize, Serialize)]
            #[serde(crate = "odata_client::serde")]
            pub struct TypeTwo {}
        }
    }
}
pub mod aliased_n_s {
    use super::SERVICE_URL;
    #[allow(unused_imports)]
    use odata_client::{
        chrono::{DateTime, FixedOffset, NaiveDate, NaiveTime},
        deserialize_with, iso8601,
        serde::{Deserialize, Serialize},
        serde_with::serde_as,
        uuid, EntityProperties, EntitySetEndpoint, ExpandQuery, FullEntityLink, SingletonEndpoint,
        StubEntityLink,
    };
    use std::marker::PhantomData;
    #[serde_as(crate = "odata_client::serde_with")]
    #[derive(Debug, Deserialize, Serialize)]
    #[serde(crate = "odata_client::serde")]
    pub struct TypeThree {}
}
