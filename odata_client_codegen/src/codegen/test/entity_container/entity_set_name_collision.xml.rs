#[allow(unused_imports)]
use odata_client::{
    chrono::{DateTime, FixedOffset, NaiveDate, NaiveTime},
    deserialize_with, iso8601,
    serde::{Deserialize, Serialize},
    serde_with::serde_as,
    uuid, EntityProperties, EntitySetEndpoint, ExpandQuery, FullEntityLink, SingletonEndpoint,
    StubEntityLink,
};
use std::marker::PhantomData;
pub const SERVICE_URL: &'static str = "http://services.odata.org/V4/TripPinService";
#[allow(dead_code)]
pub const SOME_TYPE_SET: EntitySetEndpoint<SomeType> = EntitySetEndpoint {
    service_url: SERVICE_URL,
    name: "someTypeSet",
    marker: PhantomData,
};
#[allow(dead_code)]
pub const SOME_TYPE_SET_: EntitySetEndpoint<SomeType> = EntitySetEndpoint {
    service_url: SERVICE_URL,
    name: "SomeTypeSet",
    marker: PhantomData,
};
#[serde_as(crate = "odata_client::serde_with")]
#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "odata_client::serde")]
pub struct SomeType {
    #[serde(rename = "Id")]
    pub id: i32,
}
impl EntityProperties for SomeType {
    const EXPAND_QUERY: ExpandQuery = ExpandQuery::None;
}
