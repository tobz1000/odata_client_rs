#[allow(unused_imports)]
use odata_client::{
    chrono::{DateTime, FixedOffset, NaiveDate, NaiveTime},
    deserialize_with, iso8601,
    serde::{Deserialize, Serialize},
    serde_with::serde_as,
    uuid, EntityProperties, EntitySetEndpoint, ExpandQuery, FullEntityLink, SingletonEndpoint,
    StubEntityLink,
};
use std::marker::PhantomData;
pub const SERVICE_URL: &'static str = "http://services.odata.org/V4/TripPinService";
#[serde_as(crate = "odata_client::serde_with")]
#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "odata_client::serde")]
pub struct SomeComplexType {
    pub first_field: Option<String>,
    pub second_field: String,
}
#[serde_as(crate = "odata_client::serde_with")]
#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "odata_client::serde")]
pub enum SomeEnum {
    #[serde(rename = "first_value")]
    FirstValue = 1,
    #[serde(rename = "second_value")]
    SecondValue = 2,
    #[serde(rename = "third_value")]
    ThirdValue = 3,
}
