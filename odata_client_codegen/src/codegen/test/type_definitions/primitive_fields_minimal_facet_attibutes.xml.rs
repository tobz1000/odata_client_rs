#[allow(unused_imports)]
use odata_client::{
    chrono::{DateTime, FixedOffset, NaiveDate, NaiveTime},
    deserialize_with, iso8601,
    serde::{Deserialize, Serialize},
    serde_with::serde_as,
    uuid, EntityProperties, EntitySetEndpoint, ExpandQuery, FullEntityLink, SingletonEndpoint,
    StubEntityLink,
};
use std::marker::PhantomData;
pub const SERVICE_URL: &'static str = "http://services.odata.org/V4/TripPinService";
#[serde_as(crate = "odata_client::serde_with")]
#[derive(Debug, Deserialize, Serialize)]
#[serde(crate = "odata_client::serde")]
pub struct SomeType {
    #[serde(rename = "BinaryField")]
    pub binary_field: Vec<u8>,
    #[serde(rename = "BooleanField")]
    pub boolean_field: bool,
    #[serde(rename = "ByteField")]
    pub byte_field: u8,
    #[serde(rename = "DateField")]
    pub date_field: NaiveDate,
    #[serde(rename = "DateTimeOffsetField")]
    pub date_time_offset_field: DateTime<FixedOffset>,
    #[serde(rename = "DurationField")]
    #[serde_as(as = "deserialize_with::Iso8601DurationDeserialize")]
    pub duration_field: iso8601::Duration,
    #[serde(rename = "TimeOfDayField")]
    pub time_of_day_field: NaiveTime,
    #[serde(rename = "DecimalField")]
    pub decimal_field: (i64, u64),
    #[serde(rename = "DoubleField")]
    pub double_field: f64,
    #[serde(rename = "SingleField")]
    pub single_field: f32,
    #[serde(rename = "GeographyPointField")]
    pub geography_point_field: (),
    #[serde(rename = "GeographyLineStringField")]
    pub geography_line_string_field: (),
    #[serde(rename = "GeographyPolygonField")]
    pub geography_polygon_field: (),
    #[serde(rename = "GeographyMultiPointField")]
    pub geography_multi_point_field: (),
    #[serde(rename = "GeographyMultiLineStringField")]
    pub geography_multi_line_string_field: (),
    #[serde(rename = "GeographyMultiPolygonField")]
    pub geography_multi_polygon_field: (),
    #[serde(rename = "GeographyCollectionField")]
    pub geography_collection_field: (),
    #[serde(rename = "GeometryPointField")]
    pub geometry_point_field: (),
    #[serde(rename = "GeometryLineStringField")]
    pub geometry_line_string_field: (),
    #[serde(rename = "GeometryPolygonField")]
    pub geometry_polygon_field: (),
    #[serde(rename = "GeometryMultiPointField")]
    pub geometry_multi_point_field: (),
    #[serde(rename = "GeometryMultiLineStringField")]
    pub geometry_multi_line_string_field: (),
    #[serde(rename = "GeometryMultiPolygonField")]
    pub geometry_multi_polygon_field: (),
    #[serde(rename = "GeometryCollectionField")]
    pub geometry_collection_field: (),
    #[serde(rename = "GuidField")]
    pub guid_field: uuid::Uuid,
    #[serde(rename = "Int16Field")]
    pub int16_field: i16,
    #[serde(rename = "Int32Field")]
    pub int32_field: i32,
    #[serde(rename = "Int64Field")]
    pub int64_field: i64,
    #[serde(rename = "StringField")]
    pub string_field: String,
    #[serde(rename = "SByteField")]
    pub s_byte_field: i8,
    #[serde(rename = "CollectionBinaryField")]
    pub collection_binary_field: Vec<Vec<u8>>,
    #[serde(rename = "CollectionBooleanField")]
    pub collection_boolean_field: Vec<bool>,
    #[serde(rename = "CollectionByteField")]
    pub collection_byte_field: Vec<u8>,
    #[serde(rename = "CollectionDateField")]
    pub collection_date_field: Vec<NaiveDate>,
    #[serde(rename = "CollectionDateTimeOffsetField")]
    pub collection_date_time_offset_field: Vec<DateTime<FixedOffset>>,
    #[serde(rename = "CollectionDurationField")]
    #[serde_as(as = "Vec<deserialize_with::Iso8601DurationDeserialize>")]
    pub collection_duration_field: Vec<iso8601::Duration>,
    #[serde(rename = "CollectionTimeOfDayField")]
    pub collection_time_of_day_field: Vec<NaiveTime>,
    #[serde(rename = "CollectionDecimalField")]
    pub collection_decimal_field: Vec<(i64, u64)>,
    #[serde(rename = "CollectionDoubleField")]
    pub collection_double_field: Vec<f64>,
    #[serde(rename = "CollectionSingleField")]
    pub collection_single_field: Vec<f32>,
    #[serde(rename = "CollectionGeographyPointField")]
    pub collection_geography_point_field: Vec<()>,
    #[serde(rename = "CollectionGeographyLineStringField")]
    pub collection_geography_line_string_field: Vec<()>,
    #[serde(rename = "CollectionGeographyPolygonField")]
    pub collection_geography_polygon_field: Vec<()>,
    #[serde(rename = "CollectionGeographyMultiPointField")]
    pub collection_geography_multi_point_field: Vec<()>,
    #[serde(rename = "CollectionGeographyMultiLineStringField")]
    pub collection_geography_multi_line_string_field: Vec<()>,
    #[serde(rename = "CollectionGeographyMultiPolygonField")]
    pub collection_geography_multi_polygon_field: Vec<()>,
    #[serde(rename = "CollectionGeographyCollectionField")]
    pub collection_geography_collection_field: Vec<()>,
    #[serde(rename = "CollectionGeometryPointField")]
    pub collection_geometry_point_field: Vec<()>,
    #[serde(rename = "CollectionGeometryLineStringField")]
    pub collection_geometry_line_string_field: Vec<()>,
    #[serde(rename = "CollectionGeometryPolygonField")]
    pub collection_geometry_polygon_field: Vec<()>,
    #[serde(rename = "CollectionGeometryMultiPointField")]
    pub collection_geometry_multi_point_field: Vec<()>,
    #[serde(rename = "CollectionGeometryMultiLineStringField")]
    pub collection_geometry_multi_line_string_field: Vec<()>,
    #[serde(rename = "CollectionGeometryMultiPolygonField")]
    pub collection_geometry_multi_polygon_field: Vec<()>,
    #[serde(rename = "CollectionGeometryCollectionField")]
    pub collection_geometry_collection_field: Vec<()>,
    #[serde(rename = "CollectionGuidField")]
    pub collection_guid_field: Vec<uuid::Uuid>,
    #[serde(rename = "CollectionInt16Field")]
    pub collection_int16_field: Vec<i16>,
    #[serde(rename = "CollectionInt32Field")]
    pub collection_int32_field: Vec<i32>,
    #[serde(rename = "CollectionInt64Field")]
    pub collection_int64_field: Vec<i64>,
    #[serde(rename = "CollectionStringField")]
    pub collection_string_field: Vec<String>,
    #[serde(rename = "CollectionSByteField")]
    pub collection_s_byte_field: Vec<i8>,
}
