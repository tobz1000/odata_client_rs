pub mod item_name_whitelist;

use std::collections::HashSet;

use crate::entity_model::{
    definition_lookup::{EdmItem, NamedEdmItem},
    identifiers::{QualifiedName, TargetPath},
    ComplexishType, NavigationPropertyVariant, PropertyVariant,
};
use crate::entity_model::{
    ComplexType, EntityModel, EntitySet, EntityType, EnumType, PrimitiveTypeAlias, Singleton,
};

/// Implemented by types which can specify the entity model item for which to generate client code.
/// See the [implementors](#implementors) which can be used as arguments for code generation
/// functions (e.g. [`generate_module`](crate::generate_module),
/// [`write_module_build_artifact`](crate::write_module_build_artifact)).
///
/// Choosing to not specify any filtering type will included all items in an Entity Data Model
/// document.
///
/// ## Implicit inclusions
///
/// Certain items, when explicitly included, will also implicitly pull in other items as
/// dependencies.
///
/// ### Entity Types/Complex Types
///
/// Explicitly included Entity Types and  Complex Types will implicitly include any Complex Types,
/// Enum Types, and primitive Type Definitions used in their property fields in the generated code.
///
/// Navigation property types for an Entity Type/Complex Type will be implicitly included, either as
/// a full definition, or as a "stub". The full definition is generated if the navigation property
/// is a "containment" property, or the navigation property type is explicitly included itself.
/// Navigation property types which are included as a stub will be generated as a unit struct.
///
/// Navigation property fields with stubbed entity types will be generated as `EntityLinkStub`s.
/// Navigation property fields for included entity types will be `EntityLink`s.
///
/// Any Entity/Complex types in the explicitly included type's inheritance ancestry will also be
/// included, along with their own dependencies.
///
/// ### Entity Sets/Singletons
///
/// Explicitly included Entity Sets and Singletons will implicitly include the Entity Type of the
/// set, plus the implicit dependencies for that Entity Type.
///
/// ### Enum types
/// Explicitly included enum types do not pull in any dependencies.
///
/// ### Primitive Type Definitions
/// Excplicitly included primitive type definitions do not pull in any dependencies.
pub trait EntityModelFilter {
    #[doc(hidden)]
    fn include_entity_set(&mut self, entity_set: &EntitySet<'_>) -> bool {
        let _ = entity_set;
        false
    }

    #[doc(hidden)]
    fn include_singleton(&mut self, singleton: &Singleton<'_>) -> bool {
        let _ = singleton;
        false
    }

    #[doc(hidden)]
    fn include_entity_type(&mut self, entity_type: &EntityType<'_>) -> bool {
        let _ = entity_type;
        false
    }

    #[doc(hidden)]
    fn include_complex_type(&mut self, complex_type: &ComplexType<'_>) -> bool {
        let _ = complex_type;
        false
    }

    #[doc(hidden)]
    fn include_primitive_alias(&mut self, primitive_alias: &PrimitiveTypeAlias<'_>) -> bool {
        let _ = primitive_alias;
        false
    }

    #[doc(hidden)]
    fn include_enum_type(&mut self, enum_type: &EnumType<'_>) -> bool {
        let _ = enum_type;
        false
    }
}

/// Provides an `includes` function to check whether an item should be included in generated code,
/// based on an `EntityModelFilter`.
pub enum EntityModelFilterLookup<'ar> {
    IncludeAll,
    Filter(FilteredEntityModel<'ar>),
}

impl<'ar> EntityModelFilterLookup<'ar> {
    pub fn new(
        entity_model: &EntityModel<'ar>,
        entity_model_filter: Option<&mut dyn EntityModelFilter>,
    ) -> Self {
        match entity_model_filter {
            Some(filter) => {
                let filtered_entity_model = FilteredEntityModel::new(entity_model, filter);
                EntityModelFilterLookup::Filter(filtered_entity_model)
            }
            None => EntityModelFilterLookup::IncludeAll,
        }
    }

    /// Checks whether an entity model item definition should be included in generated code.
    pub fn includes<I: EntityModelIncludes<'ar>>(&self, item: &I) -> I::Result {
        I::includes(item.name(), self)
    }
}

pub enum EntityTypeInclusion {
    IncludeFull,
    IncludeStub,
    Exclude,
}

#[derive(Debug)]
pub struct FilteredEntityModel<'ar> {
    entity_sets: HashSet<TargetPath<'ar>>,
    singletons: HashSet<TargetPath<'ar>>,
    entity_types: HashSet<QualifiedName<'ar>>,
    entity_type_stubs: HashSet<QualifiedName<'ar>>,
    complex_types: HashSet<QualifiedName<'ar>>,
    primitive_aliases: HashSet<QualifiedName<'ar>>,
    enum_types: HashSet<QualifiedName<'ar>>,
}

impl<'ar> FilteredEntityModel<'ar> {
    fn new(
        entity_model: &EntityModel<'ar>,
        entity_model_filter: &mut dyn EntityModelFilter,
    ) -> Self {
        let mut filtered_entity_model = FilteredEntityModel {
            entity_sets: HashSet::new(),
            singletons: HashSet::new(),
            entity_types: HashSet::new(),
            entity_type_stubs: HashSet::new(),
            complex_types: HashSet::new(),
            primitive_aliases: HashSet::new(),
            enum_types: HashSet::new(),
        };

        for schema in &entity_model.schemas {
            for entity_set in &schema.entity_container.entity_sets {
                if entity_model_filter.include_entity_set(entity_set) {
                    filtered_entity_model.add_entity_set(entity_set);
                }
            }

            for singleton in &schema.entity_container.singletons {
                if entity_model_filter.include_singleton(singleton) {
                    filtered_entity_model.add_singleton(singleton);
                }
            }

            for entity_type in &schema.entity_types {
                if entity_model_filter.include_entity_type(entity_type) {
                    filtered_entity_model.add_entity_type(entity_type);
                }
            }

            for complex_type in &schema.complex_types {
                if entity_model_filter.include_complex_type(complex_type) {
                    filtered_entity_model.add_complex_type(complex_type);
                }
            }

            for primitive_alias in &schema.primitive_aliases {
                if entity_model_filter.include_primitive_alias(primitive_alias) {
                    filtered_entity_model.add_primitive_alias(primitive_alias);
                }
            }

            for enum_type in &schema.enum_types {
                if entity_model_filter.include_enum_type(enum_type) {
                    filtered_entity_model.add_enum_type(enum_type);
                }
            }
        }

        filtered_entity_model
    }

    fn add_entity_set(&mut self, entity_set: &EntitySet<'ar>) {
        if self.entity_sets.contains(&entity_set.name()) {
            return;
        }

        self.entity_sets.insert(entity_set.name());
        self.add_entity_type(entity_set.entity_type.get());
    }

    fn add_singleton(&mut self, singleton: &Singleton<'ar>) {
        if self.singletons.contains(&singleton.name()) {
            return;
        }

        self.singletons.insert(singleton.name());
        self.add_entity_type(singleton.entity_type.get());
    }

    fn add_entity_type(&mut self, entity_type: &EntityType<'ar>) {
        if self.entity_types.contains(&entity_type.name()) {
            return;
        }

        self.entity_types.insert(entity_type.name());

        if let Some(base_type) = &entity_type.complex_type_fields.base_type {
            self.add_entity_type(base_type.get());
        }

        self.check_add_complexish_type_fields(&entity_type.complex_type_fields);
    }

    fn add_complex_type(&mut self, complex_type: &ComplexType<'ar>) {
        if self.complex_types.contains(&complex_type.name()) {
            return;
        }

        self.complex_types.insert(complex_type.name());

        if let Some(base_type) = &complex_type.0.base_type {
            self.add_complex_type(base_type.get());
        }

        self.check_add_complexish_type_fields(&complex_type.0);
    }

    fn check_add_complexish_type_fields<
        BaseType: EdmItem + NamedEdmItem<Name = QualifiedName<'ar>>,
    >(
        &mut self,
        complexish_type: &ComplexishType<'ar, BaseType>,
    ) {
        for prop in &complexish_type.properties {
            match prop.r#type.get() {
                PropertyVariant::Complex(complex_type) => {
                    self.add_complex_type(complex_type);
                }
                PropertyVariant::PrimitiveAlias(primitive_alias) => {
                    self.add_primitive_alias(primitive_alias);
                }
                PropertyVariant::Enumeration { enum_type, .. } => {
                    self.add_enum_type(enum_type);
                }
                PropertyVariant::Abstract => {
                    todo!() // #16
                }
                _ => {}
            }
        }

        // Only include implementations for nav prop types if they are contained by the nav prop;
        // else include a stub
        for nav_prop in &complexish_type.nav_properties {
            match &nav_prop.r#type {
                NavigationPropertyVariant::Abstract => todo!(), // #16
                NavigationPropertyVariant::Entity(entity_type) => {
                    if nav_prop.contains_target {
                        self.add_entity_type(entity_type.get());
                    } else {
                        self.entity_type_stubs.insert(entity_type.get().name());
                    }
                }
            }
        }
    }

    fn add_primitive_alias(&mut self, primitive_alias: &PrimitiveTypeAlias<'ar>) {
        self.primitive_aliases.insert(primitive_alias.name());
    }

    fn add_enum_type(&mut self, enum_type: &EnumType<'ar>) {
        self.enum_types.insert(enum_type.name());
    }
}

pub trait EntityModelIncludes<'ar>: NamedEdmItem + 'ar {
    type Result;
    fn includes<'a>(
        item_name: <Self as NamedEdmItem>::Name,
        lookup: &'a EntityModelFilterLookup<'ar>,
    ) -> Self::Result;
}

macro_rules! impl_filt_ent_mod_incl_hashset {
    ($type:ident, $hashset_field:ident) => {
        impl<'ar> EntityModelIncludes<'ar> for $type<'ar> {
            type Result = bool;

            fn includes<'a>(
                item_name: <Self as NamedEdmItem>::Name,
                lookup: &'a EntityModelFilterLookup<'ar>,
            ) -> Self::Result {
                match lookup {
                    EntityModelFilterLookup::IncludeAll => true,
                    EntityModelFilterLookup::Filter(filtered) => {
                        filtered.$hashset_field.contains(&item_name)
                    }
                }
            }
        }
    };
}

impl_filt_ent_mod_incl_hashset!(EntitySet, entity_sets);
impl_filt_ent_mod_incl_hashset!(Singleton, singletons);
impl_filt_ent_mod_incl_hashset!(ComplexType, complex_types);
impl_filt_ent_mod_incl_hashset!(PrimitiveTypeAlias, primitive_aliases);
impl_filt_ent_mod_incl_hashset!(EnumType, enum_types);

impl<'ar> EntityModelIncludes<'ar> for EntityType<'ar> {
    type Result = EntityTypeInclusion;

    fn includes<'a>(
        item_name: <Self as NamedEdmItem>::Name,
        lookup: &'a EntityModelFilterLookup<'ar>,
    ) -> Self::Result {
        match lookup {
            EntityModelFilterLookup::IncludeAll => EntityTypeInclusion::IncludeFull,
            EntityModelFilterLookup::Filter(filtered) => {
                if filtered.entity_types.contains(&item_name) {
                    EntityTypeInclusion::IncludeFull
                } else if filtered.entity_type_stubs.contains(&item_name) {
                    EntityTypeInclusion::IncludeStub
                } else {
                    EntityTypeInclusion::Exclude
                }
            }
        }
    }
}
