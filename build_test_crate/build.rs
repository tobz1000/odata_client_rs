use odata_client_codegen::{write_module_build_artifact, ConstructConfig, ItemNameWhitelist};

fn main() {
    write_module_build_artifact(
        "./trippin.xml",
        "./target/odata_clients/trippin.rs",
        "https://services.odata.org/V4/TripPinService",
        true,
        ConstructConfig::default(),
        None,
    )
    .unwrap();

    write_module_build_artifact(
        "./complex_ref_cycle.xml",
        "./target/odata_clients/complex_ref_cycle.rs",
        "https://test/",
        true,
        ConstructConfig::default(),
        None,
    )
    .unwrap();

    write_module_build_artifact(
        "./trippin.xml",
        "./target/odata_clients/people_only.rs",
        "https://test/",
        true,
        ConstructConfig::default(),
        Some(&mut ItemNameWhitelist::new().with_entity_sets(vec!["People"])),
    )
    .unwrap();
}
