//! Test build script codegen functionality

#[path = "../target/odata_clients/trippin.rs"]
mod trippin;

// TODO #2: use mock client with prepared responses for offline tests
#[tokio::test]
async fn get_my_friends() {
    let mut client = reqwest::Client::builder()
        .danger_accept_invalid_certs(true)
        .build()
        .unwrap();

    let me = trippin::ME
        .get(&mut client)
        .await
        .expect("Failed to retrieve/deserialise `ME` entity");

    for friend_link in me.properties.friends {
        let _friend = friend_link
            .get(&mut client)
            .await
            .expect("Failed to retrieve/deserialise friend link");
    }
}
