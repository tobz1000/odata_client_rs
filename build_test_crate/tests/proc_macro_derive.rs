//! Test proc macro derive functionality

use odata_client::odata_client;

#[odata_client(
    service_url = "https://services.odata.org/V4/TripPinService/",
    metadata_xml = r#"
        <?xml version="1.0" encoding="utf-8"?>
        <edmx:Edmx Version="4.0"
            xmlns:edmx="http://docs.oasis-open.org/odata/ns/edmx">
            <edmx:DataServices>
                <Schema Namespace="Microsoft.OData.SampleService.Models.TripPin"
                    xmlns="http://docs.oasis-open.org/odata/ns/edm">
                    <EntityContainer Name="Container" />
                </Schema>
            </edmx:DataServices>
        </edmx:Edmx>
    "#
)]
mod trippin_derive_xml_literal {}

#[odata_client(
    service_url = "https://services.odata.org/V4/TripPinService/",
    metadata_filepath = "build_test_crate/trippin.xml"
)]
mod trippin_derive_file {}

// TODO #2: use mock client with prepared responses for offline tests
#[odata_client(service_url = "https://services.odata.org/V4/TripPinService/")]
mod trippin_derive_endpoint_url {}
