//! Tests that indirection is appropriately added to fields of ComplexType structs so as to compile
//! sucessfully.

#[path = "../target/odata_clients/complex_ref_cycle.rs"]
mod complex_ref_cycle;
