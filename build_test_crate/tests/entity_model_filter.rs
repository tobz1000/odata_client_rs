//! Tests that, when an entity model filter is specified, appropriate items are included to allow
//! the module to build successfully.

#[path = "../target/odata_clients/people_only.rs"]
mod complex_ref_cycle;
