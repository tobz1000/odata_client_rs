// TODO https://github.com/rust-lang/rust/issues/74773: replace with `str::split_once`/
// `str::rsplit_once` when these are stabilised.
pub trait StrSplitOnce {
    fn split_char_once<'a>(&'a self, delimiter: char) -> Option<(&'a str, &'a str)>;
    fn rsplit_char_once<'a>(&'a self, delimiter: char) -> Option<(&'a str, &'a str)>;
    fn split_str_once<'a>(&'a self, delimiter: &str) -> Option<(&'a str, &'a str)>;
    fn rsplit_str_once<'a>(&'a self, delimiter: &str) -> Option<(&'a str, &'a str)>;
}

impl StrSplitOnce for str {
    fn split_char_once<'a>(&'a self, delimiter: char) -> Option<(&'a str, &'a str)> {
        self.find(delimiter)
            .map(|pos| (&self[0..pos], &self[(pos + 1..)]))
    }

    fn rsplit_char_once<'a>(&'a self, delimiter: char) -> Option<(&'a str, &'a str)> {
        self.rfind(delimiter)
            .map(|pos| (&self[0..pos], &self[(pos + 1..)]))
    }

    fn split_str_once<'a>(&'a self, delimiter: &str) -> Option<(&'a str, &'a str)> {
        self.find(delimiter)
            .map(|pos| (&self[0..pos], &self[(pos + delimiter.len()..)]))
    }

    fn rsplit_str_once<'a>(&'a self, delimiter: &str) -> Option<(&'a str, &'a str)> {
        self.rfind(delimiter)
            .map(|pos| (&self[0..pos], &self[(pos + delimiter.len()..)]))
    }
}

#[cfg(test)]
mod test {
    use super::StrSplitOnce;

    #[test]
    fn split_char_once() {
        assert_eq!("foo/bar/baz".split_char_once('/'), Some(("foo", "bar/baz")));
        assert_eq!("foo/bar/baz".split_char_once('.'), None);
    }

    #[test]
    fn rsplit_char_once() {
        assert_eq!("foo/bar/baz".split_char_once('/'), Some(("foo", "bar/baz")));
        assert_eq!("foo/bar/baz".split_char_once('.'), None);
    }

    #[test]
    fn split_str_once() {
        assert_eq!(
            "foo/bar/bar/baz".split_str_once("bar"),
            Some(("foo/", "/bar/baz"))
        );
        assert_eq!("foo/bar/baz".split_str_once("qux"), None);
    }

    #[test]
    fn rsplit_str_once() {
        assert_eq!(
            "foo/bar/bar/baz".split_str_once("bar"),
            Some(("foo/", "/bar/baz"))
        );
        assert_eq!("foo/bar/baz".split_str_once("qux"), None);
    }
}
